package com.kamisoft.babynames

abstract class GherkinUnitTest {
    protected abstract fun prepareGiven(givenSteps: List<GivenStep>)

    protected open fun clearMocks() {
        onSuccessWasRun = false
        onErrorWasRun = false
        onSuccessResult = null
    }

    protected var onSuccessWasRun = false
    protected var onErrorWasRun = false
    protected var onSuccessResult: Any? = null

    protected val onSuccessEmptyCallback: () -> Unit = {
        onSuccessWasRun = true
    }

    protected val onSuccessCallback : (Any?) -> Unit = {
        onSuccessResult = it
        onSuccessWasRun = true
    }
    protected val onErrorCallback = { _: Throwable? ->
        onErrorWasRun = true
    }

    protected fun givenThat(vararg givenSteps: GivenStep, whenThen: () -> Unit) {
        prepareGiven(givenSteps.toList())

        whenThen()

        clearMocks()
    }
}