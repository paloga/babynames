package com.kamisoft.babynames.main_menu.presentation

import com.android.billingclient.api.BillingClient
import com.kamisoft.babynames.GherkinUnitTest
import com.kamisoft.babynames.GivenStep
import com.kamisoft.babynames.billing.BillingManager
import com.kamisoft.babynames.check_app_updates.CheckAppUpdate
import com.kamisoft.babynames.choose_names.domain.use_case.GetFavoriteList
import com.kamisoft.babynames.commons.Constants
import com.kamisoft.babynames.commons.domain.model.BabyName
import com.kamisoft.babynames.commons.domain.model.Favorite
import com.kamisoft.babynames.commons.domain.model.Gender
import com.kamisoft.babynames.commons.domain.model.toBabyNames
import com.kamisoft.babynames.commons.shared_preferences.PreferencesManager
import com.kamisoft.babynames.main_menu.domain.use_case.AuthenticateUser
import com.kamisoft.babynames.tracking.TrackerManager
import com.nhaarman.mockitokotlin2.*
import com.wallbox.wallboxutils.test.CoroutineTestRule
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.runBlockingTest
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.junit.MockitoJUnitRunner

@ExperimentalCoroutinesApi
@RunWith(MockitoJUnitRunner::class)
class MainPresenterTest : GherkinUnitTest() {

    @get:Rule
    val coroutineScope = CoroutineTestRule()

    private lateinit var sut: MainPresenter

    @Mock
    private lateinit var view: MainView

    @Mock
    private lateinit var authenticateUseCase: AuthenticateUser

    @Mock
    private lateinit var preferencesManager: PreferencesManager

    @Mock
    private lateinit var getFavoritesUseCase: GetFavoriteList

    @Mock
    private lateinit var checkAppUpdate: CheckAppUpdate

    @Mock
    private lateinit var billingManager: BillingManager

    @Mock
    private lateinit var trackerManager: TrackerManager

    override fun prepareGiven(givenSteps: List<GivenStep>) {
        givenSteps.map {
            when (it) {
                is GetParentsInfo.NotSetAlready -> givenParentsInfoNotSet()
                is GetParentsInfo.AlreadySet -> givenParentsInfoSet()
                is GetParentsInfo.Parent1NameAvailable -> givenParent1Available()
                is GetParentsInfo.Parent2NameAvailable -> givenParent2Available()
                is GetParentsInfo.LastGenderChosenMale -> givenLastGenderChosenMale()
                is GetParentsInfo.LastGenderChosenFemale -> givenLastGenderChosenFemale()
                is GetParentsInfo.Parent1FavoritesLoaded -> givenParent1FavoritesLoaded()
                is GetParentsInfo.Parent2FavoritesLoaded -> givenParent2FavoritesLoaded()
                is GetParentsInfo.Parent1FavoritesEmpty,
                GetParentsInfo.Parent2FavoritesEmpty -> givenFavoritesEmpty()
                is Authentication.Successful -> givenAuthSuccess()
                is Authentication.Error -> givenAuthError()
                else -> Unit
            }
        }
    }

    private sealed class GetParentsInfo : GivenStep {
        object NotSetAlready : GetParentsInfo()
        object AlreadySet : GetParentsInfo()
        object Parent1NameAvailable : GetParentsInfo()
        object Parent2NameAvailable : GetParentsInfo()
        object LastGenderChosenMale : GetParentsInfo()
        object LastGenderChosenFemale : GetParentsInfo()
        object Parent1FavoritesLoaded : GetParentsInfo()
        object Parent2FavoritesLoaded : GetParentsInfo()
        object Parent1FavoritesEmpty : GetParentsInfo()
        object Parent2FavoritesEmpty : GetParentsInfo()
    }

    private sealed class Authentication : GivenStep {
        object Successful : Authentication()
        object Error : Authentication()
    }

    private fun givenParent1Available() {
        whenever(preferencesManager.getParent1()).thenReturn(DAD)
        whenever(preferencesManager.getParent1Name()).thenReturn(PARENT1_NAME)
    }

    private fun givenParent2Available() {
        whenever(preferencesManager.getParent2()).thenReturn(MOM)
        whenever(preferencesManager.getParent2Name()).thenReturn(PARENT2_NAME)
    }

    private fun givenLastGenderChosenMale() {
        whenever(preferencesManager.getLastGenderChosen()).thenReturn(Gender.MALE.name)
    }

    private fun givenLastGenderChosenFemale() {
        whenever(preferencesManager.getLastGenderChosen()).thenReturn(Gender.FEMALE.name)
    }

    private fun givenParentsInfoNotSet() {
        whenever(preferencesManager.getParentNamesSetDatetime()).thenReturn(0)
    }

    private fun givenParentsInfoSet() {
        whenever(preferencesManager.getParentNamesSetDatetime()).thenReturn(ANY_TIMESTAMP)
    }

    private fun givenParent1FavoritesLoaded() = coroutineScope.runBlockingTest {
        whenever(getFavoritesUseCase(any(), any())).thenReturn(PARENT1_FAVORITES)
    }

    private fun givenParent2FavoritesLoaded() = coroutineScope.runBlockingTest {
        whenever(getFavoritesUseCase(any(), any())).thenReturn(PARENT2_FAVORITES)
    }

    private fun givenFavoritesEmpty() = coroutineScope.runBlockingTest {
        whenever(getFavoritesUseCase(any(), any())).thenReturn(emptyList())
    }

    private fun givenAuthSuccess() = coroutineScope.runBlockingTest {
        whenever(authenticateUseCase.checkAuth(any())).thenReturn(true)
    }

    private fun givenAuthError() = coroutineScope.runBlockingTest {
        whenever(authenticateUseCase.checkAuth(any())).thenReturn(false)
    }

    private fun `when on init event is received`() {
        sut.onInit()
    }

    private fun `when loading favorites`() {
        sut.loadFavorites()
    }

    private fun `when parent1 favorites clicked`(favoriteList: List<BabyName>) {
        sut.onParent1FavoritesClicked(favoriteList)
    }

    private fun `when parent2 favorites clicked`(favoriteList: List<BabyName>) {
        sut.onParent2FavoritesClicked(favoriteList)
    }

    private fun `when boy button clicked`() {
        sut.onBoyClicked()
    }

    private fun `when girl button clicked`() {
        sut.onGirlClicked()
    }

    private fun `when parent1 choose favorite names clicked`() {
        sut.onParent1ChooseFavNamesClicked(Gender.FEMALE)
    }

    private fun `when parent2 choose favorite names clicked`() {
        sut.onParent2ChooseFavNamesClicked(Gender.FEMALE)
    }

    private fun `when matches clicked`() {
        sut.onMatchesClicked()
    }

    private fun `when drawer parent item clicked`() {
        sut.onDrawerItemDadMomClicked()
    }

    private fun `when drawer boy names item clicked`() {
        sut.onDrawerItemBoyNamesListClicked()
    }

    private fun `when drawer girl names item clicked`() {
        sut.onDrawerItemGirlNameListClicked()
    }

    private fun `when drawer contact item clicked`() {
        sut.onDrawerItemContactClicked()
    }

    private fun `when drawer remove ads item clicked`() {
        sut.onDrawerItemRemoveAdsClicked()
    }

    private fun `when drawer privacy policy item clicked`() {
        sut.onDrawerItemPrivacyPolicyClicked()
    }

    private fun `when no purchases done`() {
        sut.onPurchasesUpdated(emptyList())
    }

    private fun `when parent screen results returned`() {
        sut.onParentsScreenResult()
    }

    private fun `when downloading app update in progress`() {
        sut.onDownloadingUpdate(ANY_BYTES_DOWNLOADED, ANY_TOTAL_BYTES_TO_DOWNLOAD)
    }

    private fun `when app update downloaded`() {
        sut.onUpdateDownloaded()
    }

    private fun `when restart to update app clicked`() {
        sut.onRestartToUpdateAppClicked()
    }

    private fun `then verify views are initialized`() {
        verify(view).initViews()
    }

    private fun `then verify loading is shown`() {
        verify(view).showLoading()
    }

    private fun `then verify loading is hidden`() {
        verify(view).hideLoading()
    }

    private fun `then verify view state is boy state`() {
        verify(view).itsABoyState()
    }

    private fun `then verify view state is girl state`() {
        verify(view).itsAGirlState()
    }

    private fun `then verify set parent names view is opened`() {
        verify(view).openParentNamesActivity()
        verifyNoMoreInteractions(view)
    }

    private fun `then verify parent1 favorites counter is hidden`() {
        verify(view).hideParent1FavoriteCounter()
    }

    private fun `then verify parent2 favorites counter is hidden`() {
        verify(view).hideParent2FavoriteCounter()
    }

    private fun `then verify parent1 favorites counter is shown`() {
        verify(view).showParent1FavoriteCounter(PARENT1_FAVORITES.toBabyNames())
    }

    private fun `then verify parent2 favorites counter is shown`() {
        verify(view).showParent2FavoriteCounter(PARENT2_FAVORITES.toBabyNames())
    }

    private fun `then verify check app updates is called`() {
        verify(checkAppUpdate, times(1)).checkUpdateAvailable()
    }

    private fun `then verify auth error is shown`() {
        verify(view).showAuthError()
    }

    private fun `then verify parent1 favorites are shown`() {
        verify(view).showParent1FavoritesView(PARENT1_NAME, PARENT1_FAVORITES.toBabyNames(), removeAds = false)
    }

    private fun `then verify parent2 favorites are shown`() {
        verify(view).showParent2FavoritesView(PARENT2_NAME, PARENT2_FAVORITES.toBabyNames(), removeAds = false)
    }

    private fun `then verify choose names activity for parent 1 is shown`() {
        verify(view).openChooseNamesListActivity(Gender.FEMALE, PARENT1_NAME, removeAds = false)
    }

    private fun `then verify choose names activity for parent 2 is shown`() {
        verify(view).openChooseNamesListActivity(Gender.FEMALE, PARENT2_NAME, removeAds = false)
    }

    private fun `then verify interstitial ad is shown`() {
        verify(view).showInterstitialAd()
    }

    private fun `then verify matches activity is shown`() {
        verify(view).openFindMatchesActivity(Gender.FEMALE, removeAds = false)
    }

    private fun `then verify boy names list activity is shown`() {
        verify(view).openNamesListActivity(Gender.MALE, removeAds = false)
    }

    private fun `then verify girl names list activity is shown`() {
        verify(view).openNamesListActivity(Gender.FEMALE, removeAds = false)
    }

    private fun `then verify contact activity is shown`() {
        verify(view).openContactActivity()
    }

    private fun `then verify remove ads purchase flow is called`() {
        verify(billingManager, times(1)).initiatePurchaseFlow(
            Constants.BillingProductsIds.PREMIUM,
            BillingClient.SkuType.INAPP
        )
    }

    private fun `then verify privacy view is shown`() {
        verify(view).openPrivacyPolicyLink()
    }

    private fun `then verify ads are loaded`() {
        verify(view).loadAds()
    }

    private fun `then verify view is closed`() {
        verify(view).close()
    }

    private fun `then verify app update download is in progress`() {
        verify(view).showAppUpdateDownloadProgress(ANY_BYTES_DOWNLOADED.toInt(), ANY_TOTAL_BYTES_TO_DOWNLOAD.toInt())
    }

    private fun `then verify app update was downloaded message is shown`() {
        verify(view).showAppUpdateWasDownloaded()
    }

    private fun `then verify restart app to update is called`() {
        verify(checkAppUpdate, times(1)).restartAppToUpdateIt()
    }

    @Before
    fun setUp() {
        sut = MainPresenter(
            view,
            authenticateUseCase,
            preferencesManager,
            getFavoritesUseCase,
            checkAppUpdate,
            billingManager,
            trackerManager
        )
    }

    @Test
    fun `WHEN on init event is received but parents have not set their names already THEN verify set parent names view is opened`() {
        givenThat(
            GetParentsInfo.NotSetAlready
        ) {
            `when on init event is received`()
            `then verify views are initialized`()
            `then verify set parent names view is opened`()
        }
    }

    @Test
    fun `WHEN on init event is received THEN verify views are initialized`() {
        givenThat {
            `when on init event is received`()
            `then verify views are initialized`()
        }
    }

    @Test
    fun `WHEN on init event is received and parents info is set and chosen gender is male THEN verify view state is boy state`() {
        givenThat(
            GetParentsInfo.AlreadySet,
            GetParentsInfo.Parent1NameAvailable,
            GetParentsInfo.Parent2NameAvailable,
            GetParentsInfo.LastGenderChosenMale
        ) {
            `when on init event is received`()
            `then verify view state is boy state`()
        }
    }

    @Test
    fun `WHEN on init event is received and parents info is set and chosen gender is female THEN verify view state is boy state`() {
        givenThat(
            GetParentsInfo.AlreadySet,
            GetParentsInfo.Parent1NameAvailable,
            GetParentsInfo.Parent2NameAvailable,
            GetParentsInfo.LastGenderChosenFemale
        ) {
            `when on init event is received`()
            `then verify view state is girl state`()
        }
    }

    @Test
    fun `WHEN on init event is received and parents info is set and parent 1 has favorites chosen THEN verify parent1 favorites counter is shown`() {
        givenThat(
            GetParentsInfo.AlreadySet,
            GetParentsInfo.Parent1NameAvailable,
            GetParentsInfo.Parent2NameAvailable,
            GetParentsInfo.LastGenderChosenFemale,
            GetParentsInfo.Parent1FavoritesLoaded
        ) {
            `when on init event is received`()
            `then verify parent1 favorites counter is shown`()
        }
    }

    @Test
    fun `WHEN on init event is received and parents info is set and parent 2 has favorites chosen THEN verify parent1 favorites counter is shown`() {
        givenThat(
            GetParentsInfo.AlreadySet,
            GetParentsInfo.Parent1NameAvailable,
            GetParentsInfo.Parent2NameAvailable,
            GetParentsInfo.LastGenderChosenFemale,
            GetParentsInfo.Parent2FavoritesLoaded
        ) {
            `when on init event is received`()
            `then verify parent2 favorites counter is shown`()
        }
    }

    @Test
    fun `WHEN on init event is received and parents info is set and parent 1 has no favorites chosen THEN verify parent1 favorites counter is shown`() {
        givenThat(
            GetParentsInfo.AlreadySet,
            GetParentsInfo.Parent1NameAvailable,
            GetParentsInfo.Parent2NameAvailable,
            GetParentsInfo.LastGenderChosenFemale,
            GetParentsInfo.Parent1FavoritesEmpty
        ) {
            `when on init event is received`()
            `then verify parent1 favorites counter is hidden`()
        }
    }

    @Test
    fun `WHEN on init event is received and parents info is set and parent 2 has no favorites chosen THEN verify parent1 favorites counter is shown`() {
        givenThat(
            GetParentsInfo.AlreadySet,
            GetParentsInfo.Parent1NameAvailable,
            GetParentsInfo.Parent2NameAvailable,
            GetParentsInfo.LastGenderChosenFemale,
            GetParentsInfo.Parent2FavoritesEmpty
        ) {
            `when on init event is received`()
            `then verify parent2 favorites counter is hidden`()
        }
    }

    @Test
    fun `WHEN on init event is received and parents info is set THEN verify check app updates is called`() {
        givenThat(
            GetParentsInfo.AlreadySet,
            GetParentsInfo.Parent1NameAvailable,
            GetParentsInfo.Parent2NameAvailable,
            GetParentsInfo.LastGenderChosenFemale
        ) {
            `when on init event is received`()
            `then verify check app updates is called`()
        }
    }

    @Test
    fun `WHEN on init event is received and auth fails THEN verify auth error is shown`() =
        coroutineScope.runBlockingTest {
            givenThat(
                GetParentsInfo.AlreadySet,
                GetParentsInfo.Parent1NameAvailable,
                GetParentsInfo.Parent2NameAvailable,
                GetParentsInfo.LastGenderChosenFemale,
                Authentication.Error
            ) {
                `when on init event is received`()
                `then verify loading is shown`()
                `then verify auth error is shown`()
                `then verify loading is hidden`()
            }
        }

    @Test
    fun `WHEN loading favorites and parent 1 has favorites chosen THEN verify parent1 favorites counter is shown`() {
        givenThat(
            GetParentsInfo.Parent1NameAvailable,
            GetParentsInfo.Parent2NameAvailable,
            GetParentsInfo.Parent1FavoritesLoaded,
            GetParentsInfo.LastGenderChosenFemale
        ) {
            `when loading favorites`()
            `then verify parent1 favorites counter is shown`()
        }
    }

    @Test
    fun `WHEN loading favorites and parent 2 has favorites chosen THEN verify parent1 favorites counter is shown`() {
        givenThat(
            GetParentsInfo.Parent1NameAvailable,
            GetParentsInfo.Parent2NameAvailable,
            GetParentsInfo.Parent2FavoritesLoaded,
            GetParentsInfo.LastGenderChosenFemale
        ) {
            `when loading favorites`()
            `then verify parent2 favorites counter is shown`()
        }
    }

    @Test
    fun `WHEN loading favorites and parent 1 has no favorites chosen THEN verify parent1 favorites counter is shown`() {
        givenThat(
            GetParentsInfo.Parent1NameAvailable,
            GetParentsInfo.Parent2NameAvailable,
            GetParentsInfo.Parent1FavoritesEmpty,
            GetParentsInfo.LastGenderChosenFemale
        ) {
            `when loading favorites`()
            `then verify parent1 favorites counter is hidden`()
        }
    }

    @Test
    fun `WHEN loading favorites and parent 2 has no favorites chosen THEN verify parent1 favorites counter is shown`() {
        givenThat(
            GetParentsInfo.Parent1NameAvailable,
            GetParentsInfo.Parent2NameAvailable,
            GetParentsInfo.Parent2FavoritesEmpty,
            GetParentsInfo.LastGenderChosenFemale
        ) {
            `when loading favorites`()
            `then verify parent2 favorites counter is hidden`()
        }
    }

    @Test
    fun `WHEN parent1 favorites clicked THEN show parent 1 favorites`() {
        givenThat(
            GetParentsInfo.Parent1NameAvailable
        ) {
            `when parent1 favorites clicked`(PARENT1_FAVORITES.toBabyNames())
            `then verify parent1 favorites are shown`()
        }
    }

    @Test
    fun `WHEN parent2 favorites clicked THEN show parent 2 favorites`() {
        givenThat(
            GetParentsInfo.Parent2NameAvailable
        ) {
            `when parent2 favorites clicked`(PARENT2_FAVORITES.toBabyNames())
            `then verify parent2 favorites are shown`()
        }
    }

    @Test
    fun `WHEN boy button clicked THEN verify view state is boy state`() {
        givenThat(
            GetParentsInfo.Parent1NameAvailable,
            GetParentsInfo.Parent2NameAvailable,
            GetParentsInfo.LastGenderChosenMale
        ) {
            `when boy button clicked`()
            `then verify view state is boy state`()
        }
    }

    @Test
    fun `WHEN girl button clicked THEN verify view state is girl state`() {
        givenThat(
            GetParentsInfo.Parent1NameAvailable,
            GetParentsInfo.Parent2NameAvailable,
            GetParentsInfo.LastGenderChosenFemale
        ) {
            `when girl button clicked`()
            `then verify view state is girl state`()
        }
    }

    @Test
    fun `WHEN parent1 choose favorite names clicked THEN open choose name list activity for parent 1`() {
        givenThat(
            GetParentsInfo.Parent1NameAvailable,
            GetParentsInfo.LastGenderChosenFemale
        ) {
            `when parent1 choose favorite names clicked`()
            `then verify choose names activity for parent 1 is shown`()
        }
    }

    @Test
    fun `WHEN parent2 choose favorite names clicked THEN open choose name list activity for parent 2`() {
        givenThat(
            GetParentsInfo.Parent2NameAvailable,
            GetParentsInfo.LastGenderChosenFemale
        ) {
            `when parent2 choose favorite names clicked`()
            `then verify choose names activity for parent 2 is shown`()
        }
    }

    @Test
    fun `WHEN matches clicked THEN show interstitial ad and open find matches activity`() {
        givenThat(
            GetParentsInfo.LastGenderChosenFemale
        ) {
            `when matches clicked`()
            `then verify interstitial ad is shown`()
            `then verify matches activity is shown`()
        }
    }

    @Test
    fun `WHEN drawer parents item clicked THEN verify set parent names view is opened`() {
        givenThat {
            `when drawer parent item clicked`()
            `then verify set parent names view is opened`()
        }
    }

    @Test
    fun `WHEN drawer boy names item clicked THEN verify boy names list activity is shown`() {
        givenThat {
            `when drawer boy names item clicked`()
            `then verify boy names list activity is shown`()
        }
    }

    @Test
    fun `WHEN drawer girl names item clicked THEN verify girl names list activity is shown`() {
        givenThat {
            `when drawer girl names item clicked`()
            `then verify girl names list activity is shown`()
        }
    }

    @Test
    fun `WHEN drawer contact item clicked THEN verify contact activity is shown`() {
        givenThat {
            `when drawer contact item clicked`()
            `then verify contact activity is shown`()
        }
    }

    @Test
    fun `WHEN drawer contact item clicked THEN verify remove ads purchase flow is called`() {
        givenThat {
            `when drawer remove ads item clicked`()
            `then verify remove ads purchase flow is called`()
        }
    }

    @Test
    fun `WHEN drawer privacy policy item clicked THEN verify privacy view is shown`() {
        givenThat {
            `when drawer privacy policy item clicked`()
            `then verify privacy view is shown`()
        }
    }

    @Test
    fun `WHEN no purchases done THEN verify ads are loaded`() {
        givenThat {
            `when no purchases done`()
            `then verify ads are loaded`()
        }
    }

    @Test
    fun `WHEN parent screen results returned THEN verify view is closed`() {
        givenThat {
            `when parent screen results returned`()
            `then verify view is closed`()
        }
    }

    @Test
    fun `WHEN app update downloaded THEN verify app update download is in progress`() {
        givenThat {
            `when downloading app update in progress`()
            `then verify app update download is in progress`()
        }
    }

    @Test
    fun `WHEN app update downloaded THEN verify app update was downloaded message is shown`() {
        givenThat {
            `when app update downloaded`()
            `then verify app update was downloaded message is shown`()
        }
    }

    @Test
    fun `WHEN restart to update app clicked THEN verify restart app to update is called`() {
        givenThat {
            `when restart to update app clicked`()
            `then verify restart app to update is called`()
        }
    }

    private companion object {
        const val ANY_TIMESTAMP = 1614007300038L
        const val DAD = "Dad"
        const val MOM = "Mom"
        const val PARENT1_NAME = "Pablo"
        const val PARENT2_NAME = "Luna"

        val PARENT1_FAVORITES =
            listOf(
                Favorite(PARENT1_NAME, Gender.FEMALE.ordinal, "Marina", "La que ama el mar", "Latin"),
                Favorite(PARENT1_NAME, Gender.FEMALE.ordinal, "Adriana", "Mujer del mar", "Latin")
            )

        val PARENT2_FAVORITES =
            listOf(
                Favorite(PARENT2_NAME, Gender.FEMALE.ordinal, "Marina", "La que ama el mar", "Latin"),
                Favorite(PARENT2_NAME, Gender.FEMALE.ordinal, "Adriana", "Mujer del mar", "Latin")
            )

        const val ANY_BYTES_DOWNLOADED = 123L
        const val ANY_TOTAL_BYTES_TO_DOWNLOAD = 246L

    }
}