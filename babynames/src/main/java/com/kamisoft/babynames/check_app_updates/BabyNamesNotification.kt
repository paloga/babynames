package com.kamisoft.babynames.check_app_updates

import androidx.core.app.NotificationCompat

data class BabyNamesNotification(
    val id: Int,
    val title: String,
    val body: String?,
    val channelId: String,
    val autoDismiss: Boolean,
    val eventTimeInMillis: Long,
    val onlyAlertOnce: Boolean = true,
    val priority: Int = NotificationCompat.PRIORITY_LOW,
    val progress: Int? = null,
    val maxProgress: Int = 0
)