package com.kamisoft.babynames.check_app_updates

import android.annotation.TargetApi
import android.app.NotificationChannel
import android.content.Context
import android.os.Build
import androidx.core.app.NotificationCompat
import androidx.core.app.NotificationManagerCompat
import com.kamisoft.babynames.R
import com.kamisoft.babynames.commons.NotificationManager

class BabyNamesNotificationManager(private val context: Context) : NotificationManager {

    @TargetApi(Build.VERSION_CODES.O)
    override fun createDefaultNotificationChannel() {
        val notificationManager =
            context.getSystemService(Context.NOTIFICATION_SERVICE) as android.app.NotificationManager
        val channelName = DEFAULT_CHANNEL_ID
        val channel = NotificationChannel(
            DEFAULT_CHANNEL_ID,
            channelName,
            android.app.NotificationManager.IMPORTANCE_HIGH
        )
        channel.enableVibration(false)
        channel.enableLights(false)
        notificationManager.createNotificationChannel(channel)
    }

    override fun showNotification(babyNamesNotification: BabyNamesNotification) {
        with(babyNamesNotification) {
            val notificationBuilder = NotificationCompat.Builder(context, channelId)
                .setContentTitle(title)
                .setContentText(body)
                .setSmallIcon(R.drawable.ic_launcher_big)
                .setShowWhen(true)
                .setWhen(eventTimeInMillis)
                .setAutoCancel(autoDismiss)

            if (progress != null) {
                notificationBuilder.setProgress(maxProgress, progress, false)
            }

            val manager = NotificationManagerCompat.from(context)
            manager.notify(id, notificationBuilder.build())
        }
    }

    companion object {
        const val DEFAULT_CHANNEL_ID = "BabyNames"
    }
}