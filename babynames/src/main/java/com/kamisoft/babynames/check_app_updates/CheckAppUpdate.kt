package com.kamisoft.babynames.check_app_updates

import android.app.Activity
import com.google.android.play.core.appupdate.AppUpdateInfo
import com.google.android.play.core.appupdate.AppUpdateManagerFactory
import com.google.android.play.core.install.InstallStateUpdatedListener
import com.google.android.play.core.install.model.AppUpdateType
import com.google.android.play.core.install.model.InstallStatus
import com.google.android.play.core.install.model.UpdateAvailability
import com.google.android.play.core.ktx.updatePriority
import com.kamisoft.babynames.commons.shared_preferences.PreferencesManager
import com.kamisoft.babynames.logger.Logger


class CheckAppUpdate(
    private val activity: Activity,
    private val preferencesManager: PreferencesManager,
    private val updatesListener: UpdatesListener
) {

    interface UpdatesListener {
        fun onDownloadingUpdate(bytesDownloaded: Long, totalBytesToDownload: Long)
        fun onUpdateDownloaded()
    }

    private val appUpdateManager by lazy { AppUpdateManagerFactory.create(activity) }

    private val listener = InstallStateUpdatedListener { state ->
        when {
            state.installStatus() == InstallStatus.DOWNLOADING -> {
                val bytesDownloaded = state.bytesDownloaded()
                val totalBytesToDownload = state.totalBytesToDownload()

                Logger.debug("An update is being downloaded: $bytesDownloaded/$totalBytesToDownload")
                updatesListener.onDownloadingUpdate(bytesDownloaded, totalBytesToDownload)
            }
            state.installStatus() == InstallStatus.DOWNLOADED -> {
                val bytesDownloaded = state.bytesDownloaded()
                val totalBytesToDownload = state.totalBytesToDownload()
                Logger.debug("An update is being downloaded: $bytesDownloaded/$totalBytesToDownload")
                updatesListener.onDownloadingUpdate(bytesDownloaded, totalBytesToDownload)

                Logger.debug("An update has been downloaded")
                updatesListener.onUpdateDownloaded()
            }
        }
    }

    fun checkUpdateAvailable() {
        val appUpdateInfoTask = appUpdateManager.appUpdateInfo

        Logger.debug("Checking for updates")
        appUpdateInfoTask.addOnSuccessListener { appUpdateInfo ->
            when {
                appUpdateInfo.installStatus() == InstallStatus.DOWNLOADED -> updatesListener.onUpdateDownloaded()
                shouldLaunchFlexibleUpdateFlow(appUpdateInfo) -> {
                    appUpdateManager.registerListener(listener)
                    appUpdateManager.startUpdateFlowForResult(
                        appUpdateInfo,
                        AppUpdateType.FLEXIBLE,
                        activity,
                        UPDATE_REQUEST
                    )
                    preferencesManager.setLastNewVersionCheckDate(System.currentTimeMillis())
                    /* TODO [Paloga] AppUpdateType.INMEDIATE could be used to force an update depending on the priority.
                        The problem at this moment is that priority can only be set using Google Play Developer Api.
                        I will take a look into it when priority can be set in Play Developer console.                         */
                    Logger.debug("Update available: priority ${appUpdateInfo.updatePriority}")
                }
                else -> {
                    Logger.debug("No Update available")
                }
            }
        }
    }

    private fun shouldLaunchFlexibleUpdateFlow(appUpdateInfo: AppUpdateInfo): Boolean {
        val lastNewVersionCheckDate = preferencesManager.getLastNewVersionCheckDate()

        return appUpdateInfo.updateAvailability() == UpdateAvailability.UPDATE_AVAILABLE &&
            appUpdateInfo.isUpdateTypeAllowed(AppUpdateType.FLEXIBLE) &&
            isOneWeekOrMoreDifference(lastNewVersionCheckDate, System.currentTimeMillis())
    }

    fun restartAppToUpdateIt() {
        appUpdateManager.unregisterListener(listener)
        appUpdateManager.completeUpdate()
    }

    fun onDestroy() {
        appUpdateManager.unregisterListener(listener)
    }

    private fun isOneWeekOrMoreDifference(dateTime1: Long, dateTime2: Long): Boolean {
        val oneWeekInMillis = 1000 * 60 * 60 * 24 * 7
        return dateTime2 - dateTime1 > oneWeekInMillis
    }

    private companion object {
        const val UPDATE_REQUEST = 1001
    }
}