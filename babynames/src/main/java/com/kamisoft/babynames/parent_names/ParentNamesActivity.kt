package com.kamisoft.babynames.parent_names

import android.app.Activity
import android.os.Bundle
import android.view.MenuItem
import android.widget.Button
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import com.kamisoft.babynames.R
import com.kamisoft.babynames.commons.domain.model.Parent
import com.kamisoft.babynames.commons.extensions.isEmpty
import com.kamisoft.babynames.commons.extensions.snackbar
import com.kamisoft.babynames.commons.shared_preferences.AndroidPrefsManager
import com.kamisoft.babynames.databinding.ActivityParentNamesBinding
import com.kamisoft.babynames.tracking.TrackerConstants
import com.kamisoft.babynames.tracking.TrackerManager

class ParentNamesActivity : AppCompatActivity() {

    private lateinit var binding: ActivityParentNamesBinding

    private val preferencesManager by lazy { AndroidPrefsManager(this) }
    private val trackerManager by lazy { TrackerManager(this) }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityParentNamesBinding.inflate(layoutInflater)
        setContentView(binding.root)
        trackPage()
        initViews()
    }

    private fun initViews() {
        initToolbar()

        with(binding) {
            btnParent1Dad.setOnClickListener {
                trackEvent(TrackerConstants.Label.ParentsScreen.PARENT1_DAD)
                enableDadButton(btnDad = it as Button, btnMom = btnParent1Mom)
            }
            btnParent1Mom.setOnClickListener {
                trackEvent(TrackerConstants.Label.ParentsScreen.PARENT1_MOM)
                enableMomButton(btnDad = btnParent1Dad, btnMom = it as Button)
            }
            btnParent2Dad.setOnClickListener {
                trackEvent(TrackerConstants.Label.ParentsScreen.PARENT2_DAD)
                enableDadButton(btnDad = it as Button, btnMom = btnParent2Mom)
            }
            btnParent2Mom.setOnClickListener {
                trackEvent(TrackerConstants.Label.ParentsScreen.PARENT2_MOM)
                enableMomButton(btnDad = btnParent2Dad, btnMom = it as Button)
            }
            btnGo.setOnClickListener { go() }

            edtParent1Name.setText(preferencesManager.getParent1Name())
            edtParent2Name.setText(preferencesManager.getParent2Name())
            if (preferencesManager.getParent1() == Parent.MOM.value) {
                btnParent1Mom.performClick()
            } else {
                btnParent1Dad.performClick()
            }
            if (preferencesManager.getParent2() == Parent.DAD.value) {
                btnParent2Dad.performClick()
            } else {
                btnParent2Mom.performClick()
            }
        }
    }

    private fun initToolbar() {
        val toolbar = binding.toolbar.root
        setSupportActionBar(toolbar)

        supportActionBar?.title = null
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayShowHomeEnabled(true)
        toolbar.setNavigationIcon(R.drawable.ic_close)
    }

    private fun enableDadButton(btnDad: Button, btnMom: Button) {
        btnDad.isSelected = true
        btnMom.isSelected = false
        btnDad.setBackgroundColor(ContextCompat.getColor(this, R.color.dad_color))
        btnMom.setBackgroundColor(ContextCompat.getColor(this, R.color.disable))
    }

    private fun enableMomButton(btnDad: Button, btnMom: Button) {
        btnDad.isSelected = false
        btnMom.isSelected = true
        btnDad.setBackgroundColor(ContextCompat.getColor(this, R.color.disable))
        btnMom.setBackgroundColor(ContextCompat.getColor(this, R.color.mom_color))
    }

    private fun go() {
        if (areInputNamesOK()) {
            trackEvent(TrackerConstants.Label.ParentsScreen.GO_OK)
            preferencesManager.setParent1(getParent1())
            preferencesManager.setParent1Name(binding.edtParent1Name.text.toString())
            preferencesManager.setParent2(getParent2())
            preferencesManager.setParent2Name(binding.edtParent2Name.text.toString())
            preferencesManager.setParentNamesSetDatetime(System.currentTimeMillis())
            goBackToMainActivity()
        } else {
            trackEvent(TrackerConstants.Label.ParentsScreen.GO_KO)
        }
    }

    private fun goBackToMainActivity() {
        setResult(Activity.RESULT_OK)
        finish()
    }

    private fun getParent1(): String {
        var parent = Parent.MOM
        if (binding.btnParent1Dad.isSelected) {
            parent = Parent.DAD
        }
        return parent.value
    }

    private fun getParent2(): String {
        var parent = Parent.MOM
        if (binding.btnParent2Dad.isSelected) {
            parent = Parent.DAD
        }
        return parent.value
    }

    private fun areInputNamesOK(): Boolean {
        var areOk = true
        when {
            binding.edtParent1Name.isEmpty() -> {
                areOk = false
                snackbar(binding.coordinatorParentNames, R.string.parent_names_error_first_parent_name_empty)
            }
            binding.edtParent2Name.isEmpty() -> {
                areOk = false
                snackbar(binding.coordinatorParentNames, R.string.parent_names_error_second_parent_name_empty)
            }
            binding.edtParent1Name.text.toString() == binding.edtParent2Name.text.toString() -> {
                snackbar(binding.coordinatorParentNames, R.string.parent_names_error_same_parent_names)
                areOk = false
            }
        }
        return areOk
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> {
                finish()
                return true
            }
        }

        return super.onOptionsItemSelected(item)
    }

    private fun trackPage() {
        trackerManager.sendScreen(
            TrackerConstants.Section.PARENTS_SETUP.value,
            TrackerConstants.Section.ParentsSetup.MAIN.value
        )
    }

    private fun trackEvent(label: TrackerConstants.Label.ParentsScreen) {
        trackerManager.sendEvent(
            category = TrackerConstants.Section.ParentsSetup.MAIN.value,
            action = TrackerConstants.Action.CLICK.value,
            label = label.value
        )
    }

}
