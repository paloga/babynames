package com.kamisoft.babynames.broadcast_receivers

import android.annotation.SuppressLint
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import com.kamisoft.babynames.commons.OSVersionUtils
import com.kamisoft.babynames.tracking.TrackerConstants
import com.kamisoft.babynames.tracking.TrackerManager

@SuppressLint("NewApi")
class ShareBroadcastReceiver : BroadcastReceiver() {
    override fun onReceive(context: Context, intent: Intent?) {
        if (OSVersionUtils.isLollipopMR1OrGreater) {
            val trackerManager = TrackerManager(context)
            val app: String = intent?.extras?.get(Intent.EXTRA_CHOSEN_COMPONENT).toString()
            appSharedUsing(trackerManager, app)
        }
    }

    private fun appSharedUsing(trackerManager: TrackerManager, app: String) {
        trackShareEvent(trackerManager, TrackerConstants.Label.MainScreen.SHARE, app)
    }

    private fun trackShareEvent(trackerManager: TrackerManager, type: TrackerConstants.Label.MainScreen, name: String) {
        trackerManager.sendShareEvent(type.value, name)
    }

}