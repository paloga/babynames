package com.kamisoft.babynames.app_review

import android.app.Activity
import com.google.android.play.core.review.ReviewInfo
import com.google.android.play.core.review.ReviewManagerFactory
import com.kamisoft.babynames.commons.shared_preferences.PreferencesManager

class InAppReview(
    private val activity: Activity,
    private val preferencesManager: PreferencesManager
) {

    private val manager by lazy { ReviewManagerFactory.create(activity) }

    fun requestReview() {
        val requestFlow = manager.requestReviewFlow()

        requestFlow.addOnCompleteListener { request ->
            if (request.isSuccessful && shouldLaunchRequestFlow()) {
                launchReviewFlow(request.result)
                preferencesManager.setLastAppReviewRequestDate(System.currentTimeMillis())
            }
        }
    }

    private fun launchReviewFlow(reviewInfo: ReviewInfo) {
        val flow = manager.launchReviewFlow(activity, reviewInfo)
        flow.addOnCompleteListener {
            // The flow has finished. The API does not indicate whether the user
            // reviewed or not, or even whether the review dialog was shown. Thus, no
            // matter the result, we continue our app flow.
        }
    }

    private fun shouldLaunchRequestFlow(): Boolean {
        val lastRequestDate = preferencesManager.getLastAppReviewRequestDate()
        return isOneMonthOrMoreDifference(lastRequestDate, System.currentTimeMillis())
    }

    private fun isOneMonthOrMoreDifference(dateTime1: Long, dateTime2: Long): Boolean {
        val oneMonthInMillis = 1000 * 60 * 60 * 24 * 30L
        return dateTime2 - dateTime1 > oneMonthInMillis
    }
}