package com.kamisoft.babynames.matches.di

import android.app.Activity
import com.kamisoft.babynames.matches.presentation.MatchesActivity
import com.kamisoft.babynames.matches.presentation.MatchesView
import dagger.Binds
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ActivityComponent

@InstallIn(ActivityComponent::class)
@Module
abstract class MatchesModule {
    @Binds
    abstract fun bindView(activity: MatchesActivity): MatchesView
}

@InstallIn(ActivityComponent::class)
@Module
object MatchesActivityModule {
    @Provides
    fun bindActivity(activity: Activity): MatchesActivity = activity as MatchesActivity
}