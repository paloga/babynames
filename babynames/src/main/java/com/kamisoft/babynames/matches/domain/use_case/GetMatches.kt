package com.kamisoft.babynames.matches.domain.use_case

import com.kamisoft.babynames.choose_names.domain.repository.FavoritesRepository
import com.kamisoft.babynames.commons.domain.model.BabyName
import com.kamisoft.babynames.commons.domain.model.Gender

class GetMatches(private val favoritesRepository: FavoritesRepository) {

    suspend operator fun invoke(parent1Name: String, parent2Name: String, gender: Gender): List<BabyName> {
        val parent1Favorites = loadFavorites(parent1Name, gender)
        val parent2Favorites = loadFavorites(parent2Name, gender)

        val matchesList = parent1Favorites.filter {
            val babyName1 = it
            val babyName2 = parent2Favorites.find { it.name == babyName1.name }
            return@filter babyName2 != null
        }

        return matchesList
    }

    private suspend fun loadFavorites(parentName: String, gender: Gender): List<BabyName> =
        favoritesRepository.getFavorites(parentName, gender)
            .map { BabyName(name = it.babyName, origin = it.origin, meaning = it.meaning) }
}