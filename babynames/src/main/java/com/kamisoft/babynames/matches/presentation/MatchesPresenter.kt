package com.kamisoft.babynames.matches.presentation

import com.kamisoft.babynames.app_review.InAppReview
import com.kamisoft.babynames.commons.presentation.BabyNamesPresenter
import com.kamisoft.babynames.commons.shared_preferences.PreferencesManager
import com.kamisoft.babynames.commons.domain.model.BabyName
import com.kamisoft.babynames.commons.domain.model.Gender
import com.kamisoft.babynames.commons.extensions.upperCase
import com.kamisoft.babynames.matches.domain.use_case.GetMatches
import com.kamisoft.babynames.logger.Logger
import com.kamisoft.babynames.tracking.TrackerConstants
import com.kamisoft.babynames.tracking.TrackerManager
import kotlinx.coroutines.launch
import javax.inject.Inject

class MatchesPresenter @Inject constructor(
    private val view: MatchesView?,
    private val preferencesManager: PreferencesManager,
    private val getMatchesUseCase: GetMatches,
    private val inAppReview: InAppReview,
    private val trackerManager: TrackerManager
) : BabyNamesPresenter(view) {

    private lateinit var gender: Gender

    fun onInit(gender: Gender) {
        trackPage()
        this.gender = gender

        view?.run {
            initViews()
            showLoading()
            loadMatchesList()
        }
    }

    fun loadMatches() {
        Logger.debug("Loading matches...")
        val parent1Name = preferencesManager.getParent1Name().upperCase()
        val parent2Name = preferencesManager.getParent2Name().upperCase()
        presenterScope.launch {
            val matches = getMatchesUseCase(parent1Name, parent2Name, gender)
            onMatchesLoaded(matches)
        }
    }

    private fun onMatchesLoaded(namesList: List<BabyName>) {
        Logger.debug("onMatchesLoaded: ${namesList.size} matches")
        view?.run {
            hideLoading()
            if (namesList.isNotEmpty()) {
                showMatches(namesList)
                inAppReview.requestReview()
            } else {
                showEmptyView()
            }
        }
    }

    private fun trackPage() {
        trackerManager.sendScreen(
            TrackerConstants.Section.FIND_MATCHES.value,
            TrackerConstants.Section.FindMatches.MAIN.value
        )
    }
}