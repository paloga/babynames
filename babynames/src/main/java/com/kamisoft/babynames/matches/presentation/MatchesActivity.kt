package com.kamisoft.babynames.matches.presentation

import android.app.Activity
import android.view.MenuItem
import android.view.View
import com.google.android.gms.ads.AdListener
import com.google.android.gms.ads.AdRequest
import com.kamisoft.babynames.R
import com.kamisoft.babynames.choose_names.presentation.ChooseNamesListPresenter.Mode
import com.kamisoft.babynames.choose_names.presentation.adapter.NamesAdapter
import com.kamisoft.babynames.commons.domain.model.BabyName
import com.kamisoft.babynames.commons.domain.model.Gender
import com.kamisoft.babynames.commons.domain.model.toBabyNameLikableList
import com.kamisoft.babynames.commons.extensions.gone
import com.kamisoft.babynames.commons.extensions.showWithAnimation
import com.kamisoft.babynames.commons.extensions.upperCase
import com.kamisoft.babynames.commons.extensions.visible
import com.kamisoft.babynames.commons.presentation.BabyNamesActivity
import com.kamisoft.babynames.databinding.ActivityMatchesBinding
import com.kamisoft.babynames.databinding.EmptyViewBinding
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject
import kotlin.properties.ReadOnlyProperty
import kotlin.reflect.KProperty

@AndroidEntryPoint
class MatchesActivity : BabyNamesActivity(), MatchesView {

    private lateinit var binding: ActivityMatchesBinding
    private lateinit var emptyBinding: EmptyViewBinding

    @Inject
    override lateinit var presenter: MatchesPresenter

    private val gender: Gender by GenderArgument(ARG_GENDER)
    private val removeAds: Boolean by BooleanArgument(REMOVE_ADS)

    private val matchesAdapter by lazy { NamesAdapter(Mode.JUST_NAME_LIST) {} }

    override fun initViewBinding(): View {
        binding = ActivityMatchesBinding.inflate(layoutInflater)
        val view = binding.root
        emptyBinding = EmptyViewBinding.bind(view)
        return view
    }

    override fun initPresenter() {
        presenter.onInit(gender)
    }

    override fun initViews() {
        initToolbar()
        initRecyclerView()
        initEmptyView()
        if (removeAds) {
            hideAds()
        } else {
            loadAds()
        }
        binding.btnOk.setOnClickListener { finish() }
    }


    private fun initToolbar() {
        val toolbar = binding.toolbar.root
        setSupportActionBar(toolbar)
        supportActionBar?.title = null
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayShowHomeEnabled(true)
        toolbar.setNavigationIcon(R.drawable.ic_close)
    }

    private fun initRecyclerView() {
        binding.rvList.layoutManager = androidx.recyclerview.widget.LinearLayoutManager(this)
        binding.rvList.adapter = matchesAdapter
    }

    private fun initEmptyView() {
        emptyBinding.emptyView.text = getString(R.string.matches_empty)
        emptyBinding.emptyView.gone()
    }

    override fun showLoading() {
        binding.contentView.gone()
        binding.errorView.root.gone()
        binding.loadingView.root.visible()
    }

    override fun hideLoading() {
        binding.loadingView.root.gone()
        binding.errorView.root.gone()
        binding.contentView.visible()
    }

    override fun showError() {
        binding.contentView.gone()
        binding.loadingView.root.gone()
        binding.errorView.errorView.text = getString(R.string.error_get_names)
        binding.errorView.root.visible()
    }

    override fun loadMatchesList() {
        presenter.loadMatches()
    }

    override fun showMatches(namesList: List<BabyName>) {
        binding.loadingView.root.gone()
        binding.errorView.root.gone()
        binding.contentView.visible()
        matchesAdapter.setBabyNameList(namesList.toBabyNameLikableList())
    }

    override fun onOptionsItemSelected(item: MenuItem) = when (item.itemId) {
        android.R.id.home -> {
            finish()
            true
        }
        else -> super.onOptionsItemSelected(item)
    }

    override fun showEmptyView() {
        binding.loadingView.root.gone()
        binding.errorView.root.gone()
        binding.contentView.gone()
        emptyBinding.emptyView.visible()
    }

    private class GenderArgument(private val arg: String) : ReadOnlyProperty<Activity, Gender> {
        override fun getValue(thisRef: Activity, property: KProperty<*>): Gender =
            Gender.valueOf(thisRef.intent.extras?.getString(arg)?.upperCase().orEmpty())
    }

    private class BooleanArgument(private val arg: String) : ReadOnlyProperty<Activity, Boolean> {
        override fun getValue(thisRef: Activity, property: KProperty<*>): Boolean =
            thisRef.intent.extras?.getBoolean(arg) == true
    }

    private fun loadAds() {
        val adRequest = AdRequest.Builder().build()
        binding.adView.loadAd(adRequest)
        binding.adView.adListener = object : AdListener() {
            override fun onAdLoaded() {
                super.onAdLoaded()
                binding.adView.showWithAnimation()
            }
        }
    }

    private fun hideAds() {
        binding.adView.gone()
    }

    companion object {
        const val ARG_GENDER = "gender"
        const val REMOVE_ADS = "remove_ads"
    }
}
