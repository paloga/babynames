package com.kamisoft.babynames.matches.presentation

import com.kamisoft.babynames.commons.presentation.BabyNamesView
import com.kamisoft.babynames.commons.domain.model.BabyName

interface MatchesView : BabyNamesView {

    fun initViews()

    fun showEmptyView()

    fun showLoading()

    fun hideLoading()

    fun showError()

    fun loadMatchesList()

    fun showMatches(namesList: List<BabyName>)

}