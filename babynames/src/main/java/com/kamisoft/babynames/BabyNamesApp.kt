package com.kamisoft.babynames

import android.app.Application
import com.facebook.stetho.Stetho
import com.google.android.gms.ads.MobileAds
import com.kamisoft.babynames.check_app_updates.BabyNamesNotificationManager
import com.kamisoft.babynames.commons.OSVersionUtils
import com.kamisoft.babynames.commons.extensions.DelegatesExt
import com.kamisoft.babynames.logger.Logger
import dagger.hilt.android.HiltAndroidApp

@HiltAndroidApp
class BabyNamesApp : Application() {

    override fun onCreate() {
        super.onCreate()
        instance = this
        MobileAds.initialize(applicationContext)

        if (BuildConfig.DEBUG) {
            Stetho.initializeWithDefaults(this)
            Logger.start()
        }
        if (OSVersionUtils.isOreoOrGreater) {
            createDefaultNotificationChannel()
        }
    }

    private fun createDefaultNotificationChannel() {
        BabyNamesNotificationManager(this).createDefaultNotificationChannel()
    }

    companion object {
        var instance: BabyNamesApp by DelegatesExt.notNullSingleValue()
    }

}