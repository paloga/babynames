package com.kamisoft.babynames.logger

internal interface LoggerWrapper : BasicLoggerWrapper {

    fun init()

    fun tag(tag: String): BasicLoggerWrapper
}
