package com.kamisoft.babynames.main_menu.presentation

import com.kamisoft.babynames.check_app_updates.CheckAppUpdate
import com.kamisoft.babynames.choose_names.domain.use_case.GetFavoriteList
import com.kamisoft.babynames.commons.domain.model.BabyName
import com.kamisoft.babynames.commons.domain.model.Favorite
import com.kamisoft.babynames.commons.domain.model.Gender
import com.kamisoft.babynames.commons.domain.model.Parent
import com.kamisoft.babynames.commons.domain.model.toBabyNames
import com.kamisoft.babynames.commons.extensions.upperCase
import com.kamisoft.babynames.commons.presentation.BabyNamesPresenter
import com.kamisoft.babynames.commons.shared_preferences.PreferencesManager
import com.kamisoft.babynames.main_menu.domain.use_case.AuthenticateUser
import com.kamisoft.babynames.tracking.TrackerConstants
import com.kamisoft.babynames.tracking.TrackerManager
import kotlinx.coroutines.launch
import javax.inject.Inject

//TODO It would be nice to have a BaseTrackeablePresenter or something like that
class MainPresenter @Inject constructor(
    private val view: MainView?,
    private val authenticateUseCase: AuthenticateUser,
    private val preferencesManager: PreferencesManager,
    private val getFavoritesUseCase: GetFavoriteList,
    private val checkAppUpdate: CheckAppUpdate,
    private val trackerManager: TrackerManager
) : BabyNamesPresenter(view) {

    private var removeAds = false

    fun onInit() {
        trackPage()
        view?.initViews()
        if (!areParentNamesSet()) {
            view?.openParentNamesActivity()
        } else {
            loadParentNames()
            loadLastGenderChosen()
            loadFavorites()
            checkAppUpdate.checkUpdateAvailable()
            presenterScope.launch {
                view?.showLoading()
                if (!authenticateUseCase.checkAuth(refreshToken = false)) {
                    view?.showAuthError()
                }
                view?.hideLoading()
            }
        }
    }

    private fun loadParentNames() {
        val parent1 = Parent.valueOf(preferencesManager.getParent1().upperCase())
        val parent2 = Parent.valueOf(preferencesManager.getParent2().upperCase())
        val parent1Name = preferencesManager.getParent1Name()
        val parent2Name = preferencesManager.getParent2Name()
        view?.printParentNames(parent1, parent1Name, parent2, parent2Name)
    }

    private fun loadLastGenderChosen() {
        val gender = Gender.valueOf(preferencesManager.getLastGenderChosen().upperCase())
        if (gender == Gender.MALE) {
            view?.itsABoyState()
        } else {
            view?.itsAGirlState()
        }
    }

    fun loadFavorites() {
        loadParent1Favorites()
        loadParent2Favorites()
    }

    private fun loadParent1Favorites() {
        val parent1Name = preferencesManager.getParent1Name().upperCase()
        presenterScope.launch {
            val favorites = loadFavorites(parent1Name)
            onParent1FavoritesLoaded(favorites)
        }
    }

    private fun loadParent2Favorites() {
        val parent2Name = preferencesManager.getParent2Name().upperCase()
        presenterScope.launch {
            val favorites = loadFavorites(parent2Name)
            onParent2FavoritesLoaded(favorites)
        }
    }

    private suspend fun loadFavorites(parentName: String): List<Favorite> {
        val gender = Gender.valueOf(preferencesManager.getLastGenderChosen().upperCase())
        return getFavoritesUseCase(parentName, gender)
    }

    private fun onParent1FavoritesLoaded(favoriteList: List<Favorite>) {
        if (favoriteList.isNotEmpty()) {
            view?.showParent1FavoriteCounter(favoriteList.toBabyNames())
        } else {
            view?.hideParent1FavoriteCounter()
        }
    }

    private fun onParent2FavoritesLoaded(favoriteList: List<Favorite>) {
        if (favoriteList.isNotEmpty()) {
            view?.showParent2FavoriteCounter(favoriteList.toBabyNames())
        } else {
            view?.hideParent2FavoriteCounter()
        }
    }

    fun onParent1FavoritesClicked(favoriteList: List<BabyName>) {
        trackEvent(TrackerConstants.Label.MainScreen.SHOW_PARENT1_FAVORITES)
        view?.showParent1FavoritesView(preferencesManager.getParent1Name(), favoriteList, removeAds)
    }

    fun onParent2FavoritesClicked(favoriteList: List<BabyName>) {
        trackEvent(TrackerConstants.Label.MainScreen.SHOW_PARENT2_FAVORITES)
        view?.showParent2FavoritesView(preferencesManager.getParent2Name(), favoriteList, removeAds)
    }

    fun onBoyClicked() {
        trackEvent(TrackerConstants.Label.MainScreen.BOY)
        preferencesManager.setLastGenderChosen(Gender.MALE.toString())
        loadFavorites()
        view?.itsABoyState()
    }

    fun onGirlClicked() {
        trackEvent(TrackerConstants.Label.MainScreen.GIRL)
        preferencesManager.setLastGenderChosen(Gender.FEMALE.toString())
        loadFavorites()
        view?.itsAGirlState()
    }

    fun onParent1ChooseFavNamesClicked(gender: Gender) {
        trackEvent(TrackerConstants.Label.MainScreen.PARENT1_CHOOSE)
        view?.openChooseNamesListActivity(gender, preferencesManager.getParent1Name(), removeAds)
    }

    fun onParent2ChooseFavNamesClicked(gender: Gender) {
        trackEvent(TrackerConstants.Label.MainScreen.PARENT2_CHOOSE)
        view?.openChooseNamesListActivity(gender, preferencesManager.getParent2Name(), removeAds)
    }

    fun onMatchesClicked() {
        trackEvent(TrackerConstants.Label.MainScreen.MATCHES)
        val gender = Gender.valueOf(preferencesManager.getLastGenderChosen().upperCase())
        if (!removeAds) {
            view?.showInterstitialAd()
        }
        view?.openFindMatchesActivity(gender, removeAds)

    }

    fun onDrawerItemDadMomClicked() {
        trackEvent(TrackerConstants.Label.MainScreen.DRAWER_PARENTS_SCREEN)
        view?.openParentNamesActivity()
    }

    fun onDrawerItemBoyNamesListClicked() {
        trackEvent(TrackerConstants.Label.MainScreen.DRAWER_BOY_NAMES_SCREEN)
        view?.openNamesListActivity(Gender.MALE, removeAds)
    }

    fun onDrawerItemGirlNameListClicked() {
        trackEvent(TrackerConstants.Label.MainScreen.DRAWER_GIRL_NAMES_SCREEN)
        view?.openNamesListActivity(Gender.FEMALE, removeAds)
    }

    fun onDrawerItemContactClicked() {
        trackEvent(TrackerConstants.Label.MainScreen.DRAWER_CONTACT_SCREEN)
        view?.openContactActivity()
    }

    fun onDrawerItemPrivacyPolicyClicked() {
        view?.openPrivacyPolicyLink()
    }

    private fun areParentNamesSet() = preferencesManager.getParentNamesSetDatetime() != 0L

    fun onParentsScreenResult() {
        if (areParentNamesSet()) {
            loadParentNames()
        } else {
            view?.close()
        }
    }

    fun onDownloadingUpdate(bytesDownloaded: Long, totalBytesToDownload: Long) {
        view?.showAppUpdateDownloadProgress(bytesDownloaded.toInt(), totalBytesToDownload.toInt())
    }

    fun onUpdateDownloaded() {
        view?.showAppUpdateWasDownloaded()
    }

    fun onRestartToUpdateAppClicked() {
        checkAppUpdate.restartAppToUpdateIt()
    }

    private fun trackPage() {
        trackerManager.sendScreen(
            TrackerConstants.Section.MAIN.value,
            TrackerConstants.Section.Main.MAIN.value
        )
    }

    private fun trackEvent(
        label: TrackerConstants.Label.MainScreen,
        action: TrackerConstants.Action = TrackerConstants.Action.CLICK
    ) {
        trackerManager.sendEvent(
            category = TrackerConstants.Section.Main.MAIN.value,
            action = action.value,
            label = label.value
        )
    }

    override fun onDestroy() {
        checkAppUpdate.onDestroy()
        super.onDestroy()
    }
}
