﻿package com.kamisoft.babynames.main_menu.presentation

import android.annotation.SuppressLint
import android.app.PendingIntent
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.view.animation.DecelerateInterpolator
import android.widget.Toast
import androidx.appcompat.widget.AppCompatImageView
import androidx.core.content.ContextCompat
import androidx.core.view.GravityCompat
import androidx.core.view.ViewCompat
import com.afollestad.materialdialogs.MaterialDialog
import com.afollestad.materialdialogs.customview.customView
import com.google.android.gms.ads.AdListener
import com.google.android.gms.ads.AdRequest
import com.google.android.gms.ads.LoadAdError
import com.google.android.gms.ads.interstitial.InterstitialAd
import com.google.android.gms.ads.interstitial.InterstitialAdLoadCallback
import com.google.android.material.snackbar.Snackbar
import com.kamisoft.babynames.R
import com.kamisoft.babynames.broadcast_receivers.ShareBroadcastReceiver
import com.kamisoft.babynames.check_app_updates.BabyNamesNotification
import com.kamisoft.babynames.check_app_updates.BabyNamesNotificationManager
import com.kamisoft.babynames.check_app_updates.BabyNamesNotificationManager.Companion.DEFAULT_CHANNEL_ID
import com.kamisoft.babynames.check_app_updates.CheckAppUpdate
import com.kamisoft.babynames.choose_names.presentation.ChooseNamesListActivity
import com.kamisoft.babynames.choose_names.presentation.ChooseNamesListPresenter
import com.kamisoft.babynames.commons.Constants
import com.kamisoft.babynames.commons.OSVersionUtils
import com.kamisoft.babynames.commons.domain.model.BabyName
import com.kamisoft.babynames.commons.domain.model.Gender
import com.kamisoft.babynames.commons.domain.model.Parent
import com.kamisoft.babynames.commons.extensions.gone
import com.kamisoft.babynames.commons.extensions.openActivity
import com.kamisoft.babynames.commons.extensions.openActivityForResult
import com.kamisoft.babynames.commons.extensions.openUrl
import com.kamisoft.babynames.commons.extensions.showWithAnimation
import com.kamisoft.babynames.commons.extensions.toast
import com.kamisoft.babynames.commons.extensions.visible
import com.kamisoft.babynames.commons.presentation.BabyNamesActivity
import com.kamisoft.babynames.commons.presentation.custom_view.FavoritesView
import com.kamisoft.babynames.contact.presentation.ContactActivity
import com.kamisoft.babynames.databinding.ActivityMainBinding
import com.kamisoft.babynames.matches.presentation.MatchesActivity
import com.kamisoft.babynames.parent_names.ParentNamesActivity
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject

@AndroidEntryPoint
class MainActivity : BabyNamesActivity(), MainView {

    @Inject
    override lateinit var presenter: MainPresenter

    private lateinit var binding: ActivityMainBinding

    private var interstitialAd: InterstitialAd? = null
    private val notificationManager by lazy { BabyNamesNotificationManager(this) }

    override fun initViewBinding(): View {
        binding = ActivityMainBinding.inflate(layoutInflater)
        return binding.root
    }

    override fun initPresenter() {
        presenter.onInit()
    }

    override fun onResume() {
        super.onResume()
        showWithAnimation()
    }

    override fun onPause() {
        super.onPause()
        hide()
    }

    private fun hide() {
        hideToolbar()
        hideContent()
    }

    private fun showWithAnimation() {
        animateToolbar()
        animateContent()
    }

    private fun animateToolbar() {
        val toolbar = binding.content.toolbar.root
        toolbar.translationY = -toolbar.height.toFloat()
        val toolbarTitle = toolbar.getChildAt(0) as AppCompatImageView
        toolbarTitle.translationY = -toolbar.height.toFloat()

        toolbar.animate()
            .translationY(0f)
            .setDuration(Constants.Animations.DURATION_SHORT)
            .setStartDelay(Constants.Animations.ITEM_DELAY).start()

        val animator = toolbarTitle.animate()
            .translationY(0f)
            .setDuration(Constants.Animations.DURATION_SHORT)
            .setStartDelay(Constants.Animations.ITEM_DELAY)
        animator.start()
    }

    private fun hideToolbar() {
        val toolbar = binding.content.toolbar.root
        toolbar.animate()
            .translationY(-toolbar.height.toFloat())
            .setDuration(Constants.Animations.DURATION_SHORT)
            .start()
    }

    private fun animateContent() {
        ViewCompat.animate(binding.content.rlayContent)
            .alpha(1f)
            .setStartDelay(Constants.Animations.ITEM_DELAY * 2 + 200)
            .setDuration(Constants.Animations.DURATION_MEDIUM)
            .setInterpolator(DecelerateInterpolator())
            .start()
    }

    private fun hideContent() {
        ViewCompat.animate(binding.content.rlayContent)
            .alpha(0f)
            .setDuration(Constants.Animations.DURATION_MEDIUM)
            .start()
    }

    override fun initViews() {
        initToolbar()
        initNavDrawer()
        itsAGirlState()

        binding.content.btnBoy.setOnClickListener { presenter.onBoyClicked() }
        binding.content.btnGirl.setOnClickListener { presenter.onGirlClicked() }
        binding.content.btnParent1.setOnClickListener {
            presenter.onParent1ChooseFavNamesClicked(
                getSelectedGender()
            )
        }
        binding.content.btnParent2.setOnClickListener {
            presenter.onParent2ChooseFavNamesClicked(
                getSelectedGender()
            )
        }
        binding.content.btnMatches.setOnClickListener { presenter.onMatchesClicked() }
    }

    private fun getSelectedGender(): Gender =
        if (binding.content.btnBoy.isSelected) Gender.MALE else Gender.FEMALE

    private fun initToolbar() {
        val toolbar = binding.content.toolbar.root
        setSupportActionBar(toolbar)

        supportActionBar?.title = null
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayShowHomeEnabled(true)
        toolbar.setNavigationIcon(R.drawable.ic_menu_white_24dp)
    }

    private fun initNavDrawer() {
        binding.navDrawer.setNavigationItemSelectedListener { menuItem ->
            when (menuItem.itemId) {
                R.id.drawerDadAndMom -> presenter.onDrawerItemDadMomClicked()
                R.id.drawerBoyNames -> presenter.onDrawerItemBoyNamesListClicked()
                R.id.drawerGirlNames -> presenter.onDrawerItemGirlNameListClicked()
                R.id.drawerContact -> presenter.onDrawerItemContactClicked()
                R.id.drawerPrivacyPolicy -> presenter.onDrawerItemPrivacyPolicyClicked()
            }
            binding.drawerLayout.closeDrawers()
            true
        }
    }

    private fun initInterstitialAd(adRequest: AdRequest) {
        val adUnitId = getString(R.string.banner_matches_interstitial)
        InterstitialAd.load(this, adUnitId, adRequest, object : InterstitialAdLoadCallback() {
            override fun onAdLoaded(ad: InterstitialAd) {
                super.onAdLoaded(ad)
                interstitialAd = ad
            }

            override fun onAdFailedToLoad(error: LoadAdError) {
                interstitialAd = null
                super.onAdFailedToLoad(error)
            }
        })
    }

    override fun showLoading() {
        binding.content.mainScroll.gone()
        binding.content.loadingView.root.visible()
    }

    override fun hideLoading() {
        binding.content.loadingView.root.gone()
        binding.content.mainScroll.visible()
    }

    override fun printParentNames(
        parent1: Parent,
        parent1Name: String,
        parent2: Parent,
        parent2Name: String
    ) {
        if (parent1 == Parent.DAD) {
            binding.content.txtParent1.text = getString(R.string.dad_name, parent1Name)
        } else {
            binding.content.txtParent1.text = getString(R.string.mom_name, parent1Name)
        }
        if (parent2 == Parent.DAD) {
            binding.content.txtParent2.text = getString(R.string.dad_name, parent2Name)
        } else {
            binding.content.txtParent2.text = getString(R.string.mom_name, parent2Name)
        }
    }

    override fun itsABoyState() {
        binding.content.btnBoy.isSelected = true
        binding.content.btnGirl.isSelected = false
    }

    override fun itsAGirlState() {
        binding.content.btnGirl.isSelected = true
        binding.content.btnBoy.isSelected = false
    }

    override fun openFindMatchesActivity(gender: Gender, removeAds: Boolean) {
        val params = Bundle()
        params.putString(MatchesActivity.ARG_GENDER, gender.value)
        params.putBoolean(MatchesActivity.REMOVE_ADS, removeAds)
        openActivity(MatchesActivity::class.java, params)
    }

    override fun openParentNamesActivity() =
        openActivityForResult(ParentNamesActivity::class.java, PARENTS_SCREEN)

    override fun openNamesListActivity(gender: Gender, removeAds: Boolean) {
        val params = Bundle()
        params.putString(ChooseNamesListActivity.ARG_GENDER, gender.value)
        params.putBoolean(ChooseNamesListActivity.REMOVE_ADS, removeAds)
        params.putInt(
            ChooseNamesListActivity.MODE,
            ChooseNamesListPresenter.Mode.JUST_NAME_LIST.value
        )
        openActivity(ChooseNamesListActivity::class.java, params)
    }

    override fun openChooseNamesListActivity(
        gender: Gender,
        parentName: String,
        removeAds: Boolean
    ) {
        val params = Bundle()
        params.putString(ChooseNamesListActivity.ARG_GENDER, gender.value)
        params.putString(ChooseNamesListActivity.ARG_PARENT, parentName)
        params.putBoolean(ChooseNamesListActivity.REMOVE_ADS, removeAds)
        params.putInt(
            ChooseNamesListActivity.MODE,
            ChooseNamesListPresenter.Mode.WITH_FAVORITES.value
        )
        openActivityForResult(ChooseNamesListActivity::class.java, params, CHOOSE_NAMES_SCREEN)
    }

    override fun openContactActivity() = openActivity(ContactActivity::class.java)

    override fun openPrivacyPolicyLink() = openUrl(getString(R.string.privacy_policy))

    override fun onBackPressed() {
        if (isNavigationDrawerOpen()) {
            closeNavigationDrawerIfOpen()
        } else {
            super.onBackPressed()
        }
    }

    private fun isNavigationDrawerOpen() = binding.drawerLayout.isDrawerOpen(GravityCompat.START)

    private fun closeNavigationDrawerIfOpen() {
        if (isNavigationDrawerOpen()) {
            binding.drawerLayout.closeDrawer(GravityCompat.START)
        }
    }

    override fun close() = finish()

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.menu_share, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> {
                binding.drawerLayout.openDrawer(GravityCompat.START)
                return true
            }

            R.id.menu_item_share -> {
                doShare()
                return true
            }
        }

        return super.onOptionsItemSelected(item)
    }

    override fun openPlayStoreLink() {
        val googlePlayWeplanUrl = getString(R.string.app_url_play_store)
        val intentWeb = Intent(Intent.ACTION_VIEW, Uri.parse(googlePlayWeplanUrl))
        startActivity(intentWeb)
    }

    override fun loadAds() {
        val adRequest = AdRequest.Builder().build()
        binding.content.adView.loadAd(adRequest)
        binding.content.adView.adListener = object : AdListener() {
            override fun onAdLoaded() {
                super.onAdLoaded()
                binding.content.adView.showWithAnimation()
            }
        }
        initInterstitialAd(adRequest)
    }

    override fun hideAds() {
        binding.content.adView.gone()
    }

    override fun showInterstitialAd() {
        interstitialAd?.show(this)
    }

    override fun showParent1FavoriteCounter(favorites: List<BabyName>) {
        binding.content.txtParent1Favorites.text = favorites.size.toString()
        binding.content.txtParent1Favorites.visible()
        binding.content.txtParent1Favorites.setOnClickListener {
            presenter.onParent1FavoritesClicked(
                favorites
            )
        }
    }

    override fun showParent2FavoriteCounter(favorites: List<BabyName>) {
        binding.content.txtParent2Favorites.text = favorites.size.toString()
        binding.content.txtParent2Favorites.visible()
        binding.content.txtParent2Favorites.setOnClickListener {
            presenter.onParent2FavoritesClicked(
                favorites
            )
        }
    }

    override fun showParent1FavoritesView(
        parent1Name: String,
        favorites: List<BabyName>,
        removeAds: Boolean
    ) {
        val favoritesView = FavoritesView(this, removeAds)
        val dialog =
            MaterialDialog(this).title(text = getString(R.string.favorites_title, parent1Name))
                .customView(view = favoritesView)
        favoritesView.setFavorites(favorites)
        favoritesView.onOkCallback = { dialog.dismiss() }

        dialog.show()
    }

    override fun showParent2FavoritesView(
        parent2Name: String,
        favorites: List<BabyName>,
        removeAds: Boolean
    ) {
        val favoritesView = FavoritesView(this, removeAds)
        val dialog =
            MaterialDialog(this).title(text = getString(R.string.favorites_title, parent2Name))
                .customView(view = favoritesView)
        favoritesView.setFavorites(favorites)
        favoritesView.onOkCallback = { dialog.dismiss() }

        dialog.show()
    }

    override fun hideParent1FavoriteCounter() {
        binding.content.txtParent1Favorites.gone()
    }

    override fun hideParent2FavoriteCounter() {
        binding.content.txtParent2Favorites.gone()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == PARENTS_SCREEN) {
            presenter.onParentsScreenResult()
        } else if (requestCode == CHOOSE_NAMES_SCREEN) {
            presenter.loadFavorites()
        }
    }

    @SuppressLint("NewApi")
    private fun doShare() {
        val shareIntent = Intent(Intent.ACTION_SEND)
        shareIntent.type = "text/plain"
        shareIntent.putExtra(
            Intent.EXTRA_TEXT, String.format(
                getString(R.string.share_msg),
                getString(R.string.app_name), getString(R.string.app_url_play_store)
            )
        )

        if (OSVersionUtils.isLollipopMR1OrGreater) {
            val receiverIntent = Intent(this, ShareBroadcastReceiver::class.java)
            val pendingIntent = PendingIntent.getBroadcast(
                this, 0, receiverIntent,
                PendingIntent.FLAG_CANCEL_CURRENT or PendingIntent.FLAG_IMMUTABLE
            )
            startActivity(
                Intent.createChooser(
                    shareIntent,
                    resources.getText(R.string.share),
                    pendingIntent.intentSender
                )
            )
        } else {
            startActivity(Intent.createChooser(shareIntent, resources.getText(R.string.share)))
        }
    }

    inner class AppUpdatesListener : CheckAppUpdate.UpdatesListener {
        override fun onDownloadingUpdate(bytesDownloaded: Long, totalBytesToDownload: Long) {
            presenter.onDownloadingUpdate(bytesDownloaded, totalBytesToDownload)
        }

        override fun onUpdateDownloaded() {
            presenter.onUpdateDownloaded()
        }
    }

    override fun showAuthError() {
        toast(R.string.error_default, Toast.LENGTH_LONG)
    }

    override fun showAppUpdateDownloadProgress(bytesDownloaded: Int, totalBytesToDownload: Int) {
        val progressNotification = BabyNamesNotification(
            id = APP_UPDATE_DOWNLOAD_NOTIFICATION_ID,
            title = getString(R.string.notification_app_update_title),
            body = getString(R.string.notification_app_update_in_progress),
            channelId = DEFAULT_CHANNEL_ID,
            autoDismiss = false,
            eventTimeInMillis = System.currentTimeMillis(),
            progress = bytesDownloaded,
            maxProgress = totalBytesToDownload
        )

        if (bytesDownloaded == totalBytesToDownload) {
            val completedNotification = progressNotification.copy(
                body = getString(R.string.notification_app_update_completed),
                autoDismiss = true,
                progress = 0,
                maxProgress = 0
            )
            notificationManager.showNotification(completedNotification)
        } else {
            notificationManager.showNotification(progressNotification)
        }
    }

    override fun showAppUpdateWasDownloaded() {
        Snackbar.make(
            findViewById(R.id.mainLayout),
            R.string.warning_update_downloaded,
            Snackbar.LENGTH_INDEFINITE
        ).apply {
            setAction(R.string.warning_install) { presenter.onRestartToUpdateAppClicked() }
            setActionTextColor(ContextCompat.getColor(this@MainActivity, R.color.primary))
            show()
        }
    }

    companion object Request {
        const val PARENTS_SCREEN = 1001
        const val CHOOSE_NAMES_SCREEN = 2001
        const val APP_UPDATE_DOWNLOAD_NOTIFICATION_ID = 123
    }
}
