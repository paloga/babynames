package com.kamisoft.babynames.main_menu.presentation

import com.kamisoft.babynames.commons.presentation.BabyNamesView
import com.kamisoft.babynames.commons.domain.model.BabyName
import com.kamisoft.babynames.commons.domain.model.Gender
import com.kamisoft.babynames.commons.domain.model.Parent

interface MainView : BabyNamesView {

    fun initViews()

    fun showLoading()

    fun hideLoading()

    fun printParentNames(parent1: Parent, parent1Name: String, parent2: Parent, parent2Name: String)

    fun openFindMatchesActivity(gender: Gender, removeAds: Boolean)

    fun openParentNamesActivity()

    fun openNamesListActivity(gender: Gender, removeAds: Boolean)

    fun openChooseNamesListActivity(gender: Gender, parentName: String, removeAds: Boolean)

    fun openContactActivity()

    fun openPrivacyPolicyLink()

    fun itsABoyState()

    fun itsAGirlState()

    fun showParent1FavoriteCounter(favorites: List<BabyName>)

    fun showParent2FavoriteCounter(favorites: List<BabyName>)

    fun showParent1FavoritesView(parent1Name: String, favorites: List<BabyName>, removeAds: Boolean)

    fun showParent2FavoritesView(parent2Name: String, favorites: List<BabyName>, removeAds: Boolean)

    fun hideParent1FavoriteCounter()

    fun hideParent2FavoriteCounter()

    fun openPlayStoreLink()

    fun loadAds()

    fun hideAds()

    fun showInterstitialAd()

    fun showAuthError()

    fun showAppUpdateDownloadProgress(bytesDownloaded: Int, totalBytesToDownload: Int)

    fun showAppUpdateWasDownloaded()

    fun close()
}