package com.kamisoft.babynames.main_menu.domain.use_case

import com.google.firebase.auth.FirebaseAuth
import kotlin.coroutines.resume
import kotlin.coroutines.suspendCoroutine

class AuthenticateUser(private val firebaseAuth: FirebaseAuth) {

    private var token: String? = null

    suspend fun checkAuth(refreshToken: Boolean = false): Boolean {
        return if (token == null || refreshToken) {
            signInAsAnonymousUser(refreshToken)
        } else {
            true
        }
    }

    private suspend fun signInAsAnonymousUser(refreshToken: Boolean = false): Boolean {
        return suspendCoroutine { continuation ->
            firebaseAuth.signInAnonymously().addOnCompleteListener { signInTask ->
                if (signInTask.isSuccessful) {
                    firebaseAuth.currentUser?.getIdToken(refreshToken)?.addOnCompleteListener { tokenTask ->
                        if (tokenTask.isSuccessful) {
                            token = tokenTask.result?.token
                            continuation.resume(true)
                        } else {
                            continuation.resume(false)
                        }
                    }
                } else {
                    continuation.resume(false)
                }
            }
        }
    }


}