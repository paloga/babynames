package com.kamisoft.babynames.main_menu.di

import android.app.Activity
import com.kamisoft.babynames.check_app_updates.CheckAppUpdate
import com.kamisoft.babynames.main_menu.presentation.MainActivity
import com.kamisoft.babynames.main_menu.presentation.MainView
import dagger.Binds
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ActivityComponent

@InstallIn(ActivityComponent::class)
@Module
abstract class MainModule {
    @Binds
    abstract fun bindView(activity: MainActivity): MainView
}

@InstallIn(ActivityComponent::class)
@Module
object MainActivityModule {
    @Provides
    fun bindActivity(activity: Activity): MainActivity = activity as MainActivity

    @Provides
    fun provideAppUpdateListener(activity: Activity): CheckAppUpdate.UpdatesListener =
        (activity as MainActivity).AppUpdatesListener()
}