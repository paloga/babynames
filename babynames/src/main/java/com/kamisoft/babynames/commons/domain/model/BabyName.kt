package com.kamisoft.babynames.commons.domain.model

import com.kamisoft.babynames.choose_names.domain.model.BabyNameLikable

data class BabyName(val name: String, val origin: String, val meaning: String)

fun BabyName.toBabyNameLikable() = BabyNameLikable(name, origin, meaning, liked = false)

fun List<BabyName>.toBabyNameLikableList() = map { it.toBabyNameLikable() }
