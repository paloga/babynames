package com.kamisoft.babynames.commons

import com.kamisoft.babynames.check_app_updates.BabyNamesNotification

interface NotificationManager {

    fun createDefaultNotificationChannel()

    fun showNotification(babyNamesNotification: BabyNamesNotification)
}