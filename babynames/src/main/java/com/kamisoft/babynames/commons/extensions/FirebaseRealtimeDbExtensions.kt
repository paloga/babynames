package com.kamisoft.babynames.commons.extensions

import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.DatabaseReference
import com.google.firebase.database.ValueEventListener
import com.kamisoft.babynames.commons.domain.model.BabyNamesResult
import kotlin.coroutines.resume
import kotlin.coroutines.suspendCoroutine

suspend fun DatabaseReference.singleValueEvent(): BabyNamesResult<DatabaseError, DataSnapshot> =
    suspendCoroutine { continuation ->
        val valueEventListener = object : ValueEventListener {
            override fun onCancelled(error: DatabaseError) {
                continuation.resume(BabyNamesResult.Failure(error))
            }

            override fun onDataChange(snapshot: DataSnapshot) {
                continuation.resume(BabyNamesResult.Success(snapshot))
            }
        }
        addListenerForSingleValueEvent(valueEventListener)
    }