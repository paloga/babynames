package com.kamisoft.babynames.commons.domain.model

enum class Parent(val value: String) {
    DAD("dad"), MOM("mom");

    override fun toString(): String = value
}