package com.kamisoft.babynames.commons.domain.model

data class Favorite(override val parent: String, override val gender: Int, override val babyName: String, override val meaning: String, override val origin: String) : FavoriteReadable

fun Favorite.toBabyName(): BabyName {
    return BabyName(babyName, origin, meaning)
}

fun List<Favorite>.toBabyNames(): List<BabyName> {
    return this.map { it.toBabyName() }
}