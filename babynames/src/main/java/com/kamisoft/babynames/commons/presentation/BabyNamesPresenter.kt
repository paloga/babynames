package com.kamisoft.babynames.commons.presentation

import kotlinx.coroutines.CoroutineExceptionHandler
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.SupervisorJob
import kotlin.coroutines.CoroutineContext

open class BabyNamesPresenter(private var view: BabyNamesView?) : CoroutineScope {

    private val handler = CoroutineExceptionHandler { _, _ -> }
    private val job = SupervisorJob()
    override val coroutineContext: CoroutineContext = job + handler + Dispatchers.Main
    val presenterScope: CoroutineScope
        get() = this

    open fun onDestroy() {
        view = null
    }
}