package com.kamisoft.babynames.commons

import android.os.Build

object OSVersionUtils {
    val isLollipopOrGreater = Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP
    val isLollipopMR1OrGreater = Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP_MR1
    val isOreoOrGreater = Build.VERSION.SDK_INT >= Build.VERSION_CODES.O
}
