package com.kamisoft.babynames.commons.presentation.custom_view

import android.annotation.SuppressLint
import android.content.Context
import android.view.LayoutInflater
import android.widget.RelativeLayout
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.android.gms.ads.AdListener
import com.google.android.gms.ads.AdRequest
import com.kamisoft.babynames.choose_names.presentation.ChooseNamesListPresenter.Mode
import com.kamisoft.babynames.choose_names.presentation.adapter.NamesAdapter
import com.kamisoft.babynames.commons.domain.model.BabyName
import com.kamisoft.babynames.commons.domain.model.toBabyNameLikableList
import com.kamisoft.babynames.commons.extensions.gone
import com.kamisoft.babynames.commons.extensions.showWithAnimation
import com.kamisoft.babynames.databinding.DialogFavoritesBinding

@SuppressLint("ViewConstructor")
class FavoritesView(context: Context, removeAds: Boolean) : RelativeLayout(context) {

    private var binding: DialogFavoritesBinding =
        DialogFavoritesBinding.inflate(LayoutInflater.from(context), this, true)
    var onOkCallback: (() -> Unit)? = null

    init {
        initViews()
        if (removeAds) {
            hideAds()
        } else {
            loadAds()
        }
    }

    private fun initViews() {
        initRecyclerView()
        initOkButton()
    }

    private fun initRecyclerView() {
        binding.rvList.layoutManager = LinearLayoutManager(context)
    }

    private fun initOkButton() {
        binding.btnOk.setOnClickListener { onOkCallback?.invoke() }
    }

    fun setFavorites(favorites: List<BabyName>) {
        val favoritesAdapter = NamesAdapter(Mode.JUST_NAME_LIST) {}
        favoritesAdapter.setBabyNameList(favorites.toBabyNameLikableList())
        binding.rvList.adapter = favoritesAdapter
    }

    private fun loadAds() {
        val adRequest = AdRequest.Builder().build()
        binding.adView.loadAd(adRequest)
        binding.adView.adListener = object : AdListener() {
            override fun onAdLoaded() {
                super.onAdLoaded()
                binding.adView.showWithAnimation()
            }
        }
    }

    private fun hideAds() {
        binding.adView.gone()
    }
}