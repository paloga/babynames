package com.kamisoft.babynames.commons.di

import android.content.Context
import com.kamisoft.babynames.choose_names.data.datasource.FavoritesDataFactory
import com.kamisoft.babynames.choose_names.data.datasource.NamesDataFactory
import com.kamisoft.babynames.choose_names.data.repository.FavoritesDataRepository
import com.kamisoft.babynames.choose_names.data.repository.NamesDataRepository
import com.kamisoft.babynames.choose_names.domain.repository.FavoritesRepository
import com.kamisoft.babynames.choose_names.domain.repository.NamesRepository
import com.kamisoft.babynames.commons.shared_preferences.AndroidPrefsManager
import com.kamisoft.babynames.commons.shared_preferences.PreferencesManager
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object DataModule {

    /**
     * Common Data components injected in the big app graph
     */

    @Provides
    @Singleton
    fun provideFavoritesRepository(): FavoritesRepository =
        FavoritesDataRepository(FavoritesDataFactory())

    @Provides
    @Singleton
    fun provideNamesRepository(): NamesRepository = NamesDataRepository(NamesDataFactory())

    @Provides
    fun providePreferencesManager(@ApplicationContext context: Context): PreferencesManager =
        AndroidPrefsManager(context)

}