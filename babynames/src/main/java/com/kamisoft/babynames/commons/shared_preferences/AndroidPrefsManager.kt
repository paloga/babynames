﻿package com.kamisoft.babynames.commons.shared_preferences

import android.content.Context
import android.content.SharedPreferences
import com.kamisoft.babynames.commons.Constants
import com.kamisoft.babynames.commons.domain.model.Gender

class AndroidPrefsManager(context: Context) : PreferencesManager {

    companion object {
        const val PREFS_FILE = "BabyNames"

        object Key {
            const val LAST_APP_VERSION_CHECK_DATE = "last_version_check_date"
            const val PARENT_NAMES_SET_DATETIME = "parent_names_set_datetime"
            const val PARENT1 = "parent1"
            const val PARENT1_NAME = "parent1_name"
            const val PARENT2 = "parent2"
            const val PARENT2_NAME = "parent2_name"
            const val LAST_GENDER = "last_gender"
            const val LAST_APP_REVIEW_REQUEST_DATE = "last_app_review_request_date"
        }
    }

    private val preferences: SharedPreferences = context.getSharedPreferences(PREFS_FILE, Context.MODE_PRIVATE)

    override fun getLastNewVersionCheckDate(): Long =
        preferences.getLong(Key.LAST_APP_VERSION_CHECK_DATE, Constants.NOT_SPECIFIED.toLong())

    override fun setLastNewVersionCheckDate(dateInMillis: Long) =
        preferences.edit().putLong(Key.LAST_APP_VERSION_CHECK_DATE, dateInMillis).apply()

    override fun getParentNamesSetDatetime(): Long = preferences.getLong(Key.PARENT_NAMES_SET_DATETIME, 0)

    override fun setParentNamesSetDatetime(firstUseInMillis: Long) =
        preferences.edit().putLong(Key.PARENT_NAMES_SET_DATETIME, firstUseInMillis).apply()

    override fun getParent1(): String = preferences.getString(Key.PARENT1, "dad").orEmpty()

    override fun setParent1(parent: String) = preferences.edit().putString(Key.PARENT1, parent).apply()

    override fun getParent1Name(): String = preferences.getString(Key.PARENT1_NAME, "").orEmpty()

    override fun setParent1Name(name: String) = preferences.edit().putString(Key.PARENT1_NAME, name).apply()

    override fun getParent2(): String = preferences.getString(Key.PARENT2, "mom").orEmpty()

    override fun setParent2(parent: String) = preferences.edit().putString(Key.PARENT2, parent).apply()

    override fun getParent2Name(): String = preferences.getString(Key.PARENT2_NAME, "").orEmpty()

    override fun setParent2Name(name: String) = preferences.edit().putString(Key.PARENT2_NAME, name).apply()

    override fun getLastGenderChosen(): String =
        preferences.getString(Key.LAST_GENDER, Gender.MALE.toString()).orEmpty()

    override fun setLastGenderChosen(gender: String) = preferences.edit().putString(Key.LAST_GENDER, gender).apply()

    override fun getLastAppReviewRequestDate(): Long =
        preferences.getLong(Key.LAST_APP_REVIEW_REQUEST_DATE, Constants.NOT_SPECIFIED.toLong())

    override fun setLastAppReviewRequestDate(dateInMillis: Long) =
        preferences.edit().putLong(Key.LAST_APP_REVIEW_REQUEST_DATE, dateInMillis).apply()

}