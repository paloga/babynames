package com.kamisoft.babynames.commons.domain.model

enum class Gender(val value: String) {
    FEMALE("female"), MALE("male");

    override fun toString(): String = value
}