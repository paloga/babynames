package com.kamisoft.babynames.commons.presentation.model

data class OriginFilter(val origin: String, val count: Int, var selected: Boolean = true)