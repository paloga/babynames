package com.kamisoft.babynames.commons.presentation

import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity

abstract class BabyNamesActivity: AppCompatActivity(), BabyNamesView {

    abstract val presenter: BabyNamesPresenter?

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val view = initViewBinding()
        setContentView(view)
        initPresenter()
    }

    abstract fun initViewBinding(): View

    open fun initPresenter() {
        // Nothing to do here
    }

    override fun onDestroy() {
        presenter?.onDestroy()
        super.onDestroy()
    }

}