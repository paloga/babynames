package com.kamisoft.babynames.commons

object Constants {
    const val NOT_SPECIFIED = -1
    const val TIMEOUT_DEFAULT = 30000L

    object Animations {
        const val STARTUP_DELAY = 200L
        const val DURATION_SHORT = 300L
        const val DURATION_MEDIUM = 400L
        const val DURATION_LONG = 500L
        const val ITEM_DELAY = 150L
    }

    object Ids {
        const val FILTER_SHOWCASE = "filter_showcase_01"
    }

    object BillingProductsIds {
        const val PREMIUM = "premium"
    }
}
