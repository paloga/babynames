package com.kamisoft.babynames.commons.presentation.custom_view

import android.annotation.SuppressLint
import android.content.Context
import android.view.LayoutInflater
import android.widget.RelativeLayout
import com.google.android.gms.ads.AdListener
import com.google.android.gms.ads.AdRequest
import com.kamisoft.babynames.R
import com.kamisoft.babynames.choose_names.presentation.adapter.FilterAdapter
import com.kamisoft.babynames.commons.extensions.gone
import com.kamisoft.babynames.commons.extensions.showWithAnimation
import com.kamisoft.babynames.commons.presentation.model.OriginFilter
import com.kamisoft.babynames.databinding.DialogFilterOriginBinding

@SuppressLint("ViewConstructor")
class FilterView(context: Context, removeAds: Boolean) : RelativeLayout(context) {

    private var binding: DialogFilterOriginBinding =
        DialogFilterOriginBinding.inflate(LayoutInflater.from(context), this, true)
    var onOkCallback: ((List<OriginFilter>) -> Unit)? = null

    init {
        initViews()
        if (removeAds) {
            hideAds()
        } else {
            loadAds()
        }
    }

    private fun initViews() {
        initSelectAllCheckBox()
        initRecyclerView()
        initOkButton()
    }

    private fun initSelectAllCheckBox() {
        binding.chkSelectUnSelectAll.setOnCheckedChangeListener { compoundButton, checked ->
            if (checked) {
                compoundButton.text = context.getString(R.string.filter_unselect_all)
                (binding.rvList.adapter as FilterAdapter).selectAll()
            } else {
                compoundButton.text = context.getString(R.string.filter_select_all)
                (binding.rvList.adapter as FilterAdapter).unSelectAll()
            }
        }
    }

    private fun initRecyclerView() {
        binding.rvList.layoutManager = androidx.recyclerview.widget.LinearLayoutManager(context)
    }

    private fun initOkButton() {
        binding.btnOk.setOnClickListener { onOkCallback?.invoke((binding.rvList.adapter as FilterAdapter).origins) }
    }

    fun setOrigins(origins: List<OriginFilter>) {
        val filterAdapter = FilterAdapter(origins)
        binding.rvList.adapter = filterAdapter
        binding.chkSelectUnSelectAll.isChecked = filterAdapter.areAllSelected()
    }

    private fun loadAds() {
        val adRequest = AdRequest.Builder().build()
        binding.adView.loadAd(adRequest)
        binding.adView.adListener = object : AdListener() {
            override fun onAdLoaded() {
                super.onAdLoaded()
                binding.adView.showWithAnimation()
            }
        }
    }

    private fun hideAds() {
        binding.adView.gone()
    }

}