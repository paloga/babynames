package com.kamisoft.babynames.commons.di

import android.app.Activity
import android.content.Context
import com.google.firebase.auth.FirebaseAuth
import com.kamisoft.babynames.app_review.InAppReview
import com.kamisoft.babynames.check_app_updates.CheckAppUpdate
import com.kamisoft.babynames.choose_names.domain.repository.FavoritesRepository
import com.kamisoft.babynames.choose_names.domain.repository.NamesRepository
import com.kamisoft.babynames.choose_names.domain.use_case.GetFavoriteList
import com.kamisoft.babynames.choose_names.domain.use_case.GetNameList
import com.kamisoft.babynames.choose_names.domain.use_case.SaveFavoriteName
import com.kamisoft.babynames.commons.shared_preferences.PreferencesManager
import com.kamisoft.babynames.main_menu.domain.use_case.AuthenticateUser
import com.kamisoft.babynames.matches.domain.use_case.GetMatches
import com.kamisoft.babynames.tracking.TrackerManager
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ActivityComponent
import dagger.hilt.android.qualifiers.ActivityContext

@Module
@InstallIn(ActivityComponent::class)
object DomainModule {

    /**
     * Use cases injected in the activity graph for presenter build
     */

    @Provides
    fun provideAuthenticateUserUseCase() = AuthenticateUser(FirebaseAuth.getInstance())

    @Provides
    fun provideGetFavoriteListUseCase(favoritesRepository: FavoritesRepository) = GetFavoriteList(favoritesRepository)

    @Provides
    fun provideGetNameListUseCase(namesRepository: NamesRepository) = GetNameList(namesRepository)

    @Provides
    fun provideSaveFavoriteNameUseCase(favoritesRepository: FavoritesRepository, namesRepository: NamesRepository) =
        SaveFavoriteName(favoritesRepository, namesRepository)

    @Provides
    fun providesGetMatchesUseCase(favoritesRepository: FavoritesRepository) = GetMatches(favoritesRepository)

    @Provides
    fun providesInAppReview(activity: Activity, preferenceManager: PreferencesManager) =
        InAppReview(activity, preferenceManager)

    @Provides
    fun providesCheckAppUpdate(
        activity: Activity,
        preferenceManager: PreferencesManager,
        updatesListener: CheckAppUpdate.UpdatesListener
    ) = CheckAppUpdate(activity, preferenceManager, updatesListener)

    @Provides
    fun provideTrackerManager(@ActivityContext context: Context) = TrackerManager(context)
}