package com.kamisoft.babynames.commons.extensions

import java.util.*

fun String.upperCase() = this.toUpperCase(Locale.getDefault())

fun String.lowerCase() = this.toLowerCase(Locale.getDefault())