package com.kamisoft.babynames.commons.domain.model

interface FavoriteReadable {
    val parent: String
    val gender: Int
    val babyName: String
    val meaning: String
    val origin: String
}
