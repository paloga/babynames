package com.kamisoft.babynames.commons.domain.model

/**
 * Sealed class that represents either a success or a failure, including an associated value in each case.
 * A Generic for the success case, and a _root_ide_package_.kotlin.Exception in the failure case.
 */
sealed class BabyNamesResult<out Failure, out Success> {

    /**
     * A failure, storing a Failure value.
     */
    data class Failure<out Failure>(val failure: Failure) : BabyNamesResult<Failure, Nothing>()

    /**
     * A success, storing a Success value.
     */
    data class Success<out Success>(val success: Success) : BabyNamesResult<Nothing, Success>()

    /**
     * Boolean property to know if result was a success
     */
    val isSuccess get() = this is BabyNamesResult.Success<Success>

    /**
     * Boolean property to know if result was a failure
     */
    val isFailure get() = this is BabyNamesResult.Failure<Failure>
}

/**
 * Method to build a success result
 */
fun <Input> Input.success() = BabyNamesResult.Success(this)

/**
 * Method to build a failure result
 */
fun <Input> Input.failure() = BabyNamesResult.Failure(this)

/**
 * Method to obtain and control the failure and the success cases.
 */
fun <Failure, Success, T> BabyNamesResult<Failure, Success>.fold(left: (Failure) -> T, right: (Success) -> T): T =
    when (this) {
        is BabyNamesResult.Failure -> left(failure)
        is BabyNamesResult.Success -> right(success)
    }

/**
 * Method to obtain and control the failure and the success cases with suspended functions.
 */
suspend fun <Failure, Success, T> BabyNamesResult<Failure, Success>.foldSuspended(
    left: suspend (Failure) -> T,
    right: suspend (Success) -> T
): T =
    when (this) {
        is BabyNamesResult.Failure -> left(failure)
        is BabyNamesResult.Success -> right(success)
    }

private fun <Failure, Success, T> BabyNamesResult<Failure, Success>.flatMap(f: (Success) -> BabyNamesResult<Failure, T>):
    BabyNamesResult<Failure, T> = fold({ this as BabyNamesResult.Failure }, f)

fun <Failure, Success, T> BabyNamesResult<Failure, Success>.map(f: (Success) -> T): BabyNamesResult<Failure, T> =
    flatMap { BabyNamesResult.Success(f(it)) }

/**
 * Method to obtain and control the failure case.
 */
fun <Failure, Success> BabyNamesResult<Failure, Success>.foldFailure(): Failure? =
    when (this) {
        is BabyNamesResult.Failure -> failure
        else -> null
    }

/**
 * Method to obtain and control the success case.
 */
fun <Failure, Success> BabyNamesResult<Failure, Success>.foldSuccess(): Success? =
    when (this) {
        is BabyNamesResult.Success -> success
        else -> null
    }

/**
 * Performs a logical `and` operation between this BabyNamesResult and [other].
 * Similarly to the logical `and` operator, this function does not perform short-circuit evaluation. Both `this` and
 * [other] will always be evaluated.
 * In case `this` failed, `this` will be returned otherwise [other] will be returned
 */
fun <Failure, Success> BabyNamesResult<Failure, Success>.and(
    other: BabyNamesResult<Failure, Success>
): BabyNamesResult<Failure, Success> {
    if (isFailure) return this

    return other
}

/**
 * Performs a logical `not` operation on `this`.
 * In case of failure [successInstance] will be returned. In case of success [errorInstance] will be returned.
 */
fun <Failure, Success> BabyNamesResult<Failure, Success>.not(errorInstance: Failure, successInstance: Success):
    BabyNamesResult<Failure, Success> =
        if (isFailure) successInstance.success() else errorInstance.failure()

/**
 * Provides default values for the generic method [not]. This is mainly to simplify code in common cases.
 *
 * This could be replaced by a language feature from Kotlin 1.4
 * See https://discuss.kotlinlang.org/t/allow-typealias-to-have-the-same-name-as-generic-class/6789/6?u=wasabi375
 */
fun BabyNamesResult<Exception, Unit>.not(): BabyNamesResult<Exception, Unit> =
    not(Exception(), Unit)

/**
 * Converts a [Boolean] value to a [BabyNamesResult], both [errorInstance] and [successInstance] need to be provide for
 * handling each case.
 */
fun <Failure, Success> Boolean.toBabyNamesResult(
    errorInstance: Failure,
    successInstance: Success
): BabyNamesResult<Failure, Success> =
    if (this) successInstance.success() else errorInstance.failure()

/**
 * Provides default values for the generic method [toBabyNamesResult]. This is mainly to simplify code in common cases.
 *
 * This could be replaced by a language feature from Kotlin 1.4
 * See https://discuss.kotlinlang.org/t/allow-typealias-to-have-the-same-name-as-generic-class/6789/6?u=wasabi375
 */
fun Boolean.toBabyNamesResult(): BabyNamesResult<Exception, Unit> =
    toBabyNamesResult(Exception(), Unit)

