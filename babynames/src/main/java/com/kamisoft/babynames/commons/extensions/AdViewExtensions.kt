package com.kamisoft.babynames.commons.extensions

import android.view.animation.DecelerateInterpolator
import androidx.core.view.ViewCompat
import com.google.android.gms.ads.AdView
import com.kamisoft.babynames.commons.Constants

fun AdView.showWithAnimation() {
    ViewCompat.animate(this).alpha(1f)
        .setDuration(Constants.Animations.DURATION_LONG)
        .setInterpolator(DecelerateInterpolator())
        .start()
}