package com.kamisoft.babynames.choose_names.data.datasource

class NamesDataFactory {

    fun create(): NamesDataSource = FirebaseNamesDataSource()

}