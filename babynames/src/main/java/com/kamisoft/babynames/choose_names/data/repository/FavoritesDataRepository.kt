package com.kamisoft.babynames.choose_names.data.repository

import com.kamisoft.babynames.choose_names.data.datasource.FavoritesDataFactory
import com.kamisoft.babynames.choose_names.domain.repository.FavoritesRepository
import com.kamisoft.babynames.commons.domain.model.Favorite
import com.kamisoft.babynames.commons.domain.model.FavoriteReadable
import com.kamisoft.babynames.commons.domain.model.Gender

class FavoritesDataRepository(private val favoritesDataFactory: FavoritesDataFactory) : FavoritesRepository {

    override fun isFavorite(parent: String, gender: Gender, name: String): Boolean {
        val favoritesDataSource = favoritesDataFactory.create()
        return favoritesDataSource.isFavorite(parent, gender, name)
    }

    override fun saveOrRemoveFavoriteName(favorite: Favorite) {
        val favoritesDataSource = favoritesDataFactory.create()

        if (favoritesDataSource.isFavorite(favorite.parent, Gender.values()[favorite.gender], favorite.babyName)) {
            favoritesDataSource.deleteFavorite(favorite)
        } else {
            favoritesDataSource.saveFavorite(favorite)
        }
    }

    override suspend fun getFavorites(parent: String, gender: Gender): List<FavoriteReadable> {
        val favoritesDataSource = favoritesDataFactory.create()
        return favoritesDataSource.getFavorites(parent, gender)
    }


}