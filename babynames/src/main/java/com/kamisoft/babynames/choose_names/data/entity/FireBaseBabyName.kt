package com.kamisoft.babynames.choose_names.data.entity

import com.google.firebase.database.IgnoreExtraProperties
import com.kamisoft.babynames.commons.domain.model.BabyName

@IgnoreExtraProperties
class FireBaseBabyName {
    lateinit var name: String
    lateinit var gender: String
    lateinit var origin: String
    lateinit var meaning: String
}

fun FireBaseBabyName.toBabyName(): BabyName = BabyName(name = name, origin = origin, meaning = meaning)

fun List<FireBaseBabyName>.toBabyNames(): List<BabyName> = map { it.toBabyName() }