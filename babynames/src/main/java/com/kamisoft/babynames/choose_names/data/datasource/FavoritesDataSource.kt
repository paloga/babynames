package com.kamisoft.babynames.choose_names.data.datasource

import com.kamisoft.babynames.commons.domain.model.BabyNamesResult
import com.kamisoft.babynames.commons.domain.model.FavoriteReadable
import com.kamisoft.babynames.commons.domain.model.Gender
import java.lang.Exception

interface FavoritesDataSource {

    fun isFavorite(parent: String, gender: Gender, name: String): Boolean

    fun saveFavorite(favorite: FavoriteReadable): Long

    fun deleteFavorite(favorite: FavoriteReadable)

    suspend fun getFavorites(parent: String, gender: Gender): List<FavoriteReadable>
}