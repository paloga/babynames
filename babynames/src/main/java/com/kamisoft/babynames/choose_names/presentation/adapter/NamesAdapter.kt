package com.kamisoft.babynames.choose_names.presentation.adapter

import android.text.TextUtils
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.kamisoft.babynames.R
import com.kamisoft.babynames.choose_names.domain.model.BabyNameLikable
import com.kamisoft.babynames.choose_names.presentation.ChooseNamesListPresenter
import com.kamisoft.babynames.commons.extensions.invisible
import com.kamisoft.babynames.commons.extensions.upperCase
import com.kamisoft.babynames.commons.extensions.visible
import com.kamisoft.babynames.databinding.RowNameBinding
import com.simplecityapps.recyclerview_fastscroll.views.FastScrollRecyclerView
import kotlin.properties.Delegates

class NamesAdapter(
    private val mode: ChooseNamesListPresenter.Mode,
    private val listener: (BabyNameLikable) -> Unit
) : RecyclerView.Adapter<RecyclerView.ViewHolder>(),
    FastScrollRecyclerView.SectionedAdapter {

    private var nameItemSize by Delegates.notNull<Int>()
    private var nameList: List<BabyNameLikable> = emptyList()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        nameItemSize = parent.resources.getDimensionPixelSize(R.dimen.name_item_height)
        val binding = RowNameBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return NameViewHolder(binding)
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        if (holder is NameViewHolder) {
            holder.bind(nameList[position], listener)
        }
    }

    override fun getItemCount() = nameList.size

    fun setBabyNameList(names: List<BabyNameLikable>) {
        this.nameList = names
        notifyDataSetChanged()
    }

    fun getLikedBabyNames() = nameList.filter { it.liked }

    private fun getInitialLetter(name: String) = name[0].toString()

    override fun getSectionName(position: Int): String = getInitialLetter(nameList[position].name)

    inner class NameViewHolder(val binding: RowNameBinding) : RecyclerView.ViewHolder(binding.root) {
        fun bind(babyName: BabyNameLikable, listener: (BabyNameLikable) -> Unit) = with(itemView) {
            binding.txtName.text = babyName.name
            if (!TextUtils.isEmpty(babyName.origin)) {
                binding.chkOrigin.text = binding.chkOrigin.context.getString(R.string.origin, babyName.origin)
            } else {
                binding.chkOrigin.text = ""
            }
            binding.txtMeaning.text = babyName.meaning

            if (mode == ChooseNamesListPresenter.Mode.WITH_FAVORITES) {
                binding.btnLike.visible()
                if (babyName.liked) {
                    binding.btnLike.setImageResource(R.drawable.ic_heart_red)
                } else {
                    binding.btnLike.setImageResource(R.drawable.ic_heart_outline_grey)
                }
                setOnClickListener {
                    if (!babyName.liked) {
                        notifyItemChanged(adapterPosition, ACTION_LIKE_BUTTON_CLICKED)
                    } else {
                        notifyItemChanged(adapterPosition, ACTION_UNLIKE_BUTTON_CLICKED)
                    }
                    listener(babyName)
                }
            } else {
                binding.btnLike.invisible()
            }
        }
    }

    fun getFirstItemPositionStartingWith(text: String): Int {
        return nameList.indexOfFirst { it.name.upperCase().startsWith(text.upperCase()) }
    }

    companion object {
        const val ACTION_LIKE_BUTTON_CLICKED = "action_like_button"
        const val ACTION_UNLIKE_BUTTON_CLICKED = "action_unlike_button"
    }
}