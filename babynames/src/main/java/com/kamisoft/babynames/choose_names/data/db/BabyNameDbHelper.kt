package com.kamisoft.babynames.choose_names.data.db

import android.content.Context
import android.database.sqlite.SQLiteDatabase
import com.kamisoft.babynames.BabyNamesApp
import org.jetbrains.anko.db.*

class BabyNameDbHelper(ctx: Context = BabyNamesApp.instance) : ManagedSQLiteOpenHelper(ctx, DB_NAME, null, DB_VERSION) {

    companion object {
        const val DB_NAME = "babyname.db"
        const val DB_VERSION = 2
        val instance by lazy { BabyNameDbHelper() }
    }

    override fun onCreate(db: SQLiteDatabase) {
        db.createTable(
            FavoriteTable.NAME, true,
            FavoriteTable.ID to INTEGER + PRIMARY_KEY,
            FavoriteTable.PARENT to TEXT,
            FavoriteTable.GENDER to INTEGER,
            FavoriteTable.BABY_NAME to TEXT,
            FavoriteTable.MEANING to TEXT,
            FavoriteTable.ORIGIN to TEXT
        )
    }

    override fun onUpgrade(db: SQLiteDatabase, oldVersion: Int, newVersion: Int) {
        if (oldVersion < 2) { //In version 2, we updated FavoriteTable with more fields
            db.execSQL("ALTER TABLE " + FavoriteTable.NAME + " ADD COLUMN " + FavoriteTable.MEANING + " TEXT DEFAULT ''")
            db.execSQL("ALTER TABLE " + FavoriteTable.NAME + " ADD COLUMN " + FavoriteTable.ORIGIN + " TEXT DEFAULT ''")
        }
    }

}