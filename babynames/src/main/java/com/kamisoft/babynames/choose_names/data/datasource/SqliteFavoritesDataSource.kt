package com.kamisoft.babynames.choose_names.data.datasource

import com.kamisoft.babynames.choose_names.data.db.BabyNameDbHelper
import com.kamisoft.babynames.choose_names.data.db.DbDataMapper
import com.kamisoft.babynames.choose_names.data.db.FavoriteTable
import com.kamisoft.babynames.choose_names.data.entity.SqliteFavorite
import com.kamisoft.babynames.commons.domain.model.BabyNamesResult
import com.kamisoft.babynames.commons.domain.model.FavoriteReadable
import com.kamisoft.babynames.commons.domain.model.Gender
import com.kamisoft.babynames.commons.extensions.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import org.jetbrains.anko.db.insert
import org.jetbrains.anko.db.select
import java.lang.Exception
import kotlin.coroutines.suspendCoroutine

class SqliteFavoritesDataSource(private val babyNameDbHelper: BabyNameDbHelper = BabyNameDbHelper.instance,
                                private val dataMapper: DbDataMapper = DbDataMapper()) : FavoritesDataSource {

    override fun isFavorite(parent: String, gender: Gender, name: String): Boolean {
        return babyNameDbHelper.use {
            val where = "LOWER(${FavoriteTable.PARENT}) = ? AND ${FavoriteTable.GENDER} = ? AND LOWER(${FavoriteTable.BABY_NAME}) = ?"
            val sqliteFavorite = select(FavoriteTable.NAME)
                    .whereSimple(where, parent.lowerCase(), gender.ordinal.toString(), name.lowerCase())
                    .parseOpt { SqliteFavorite(HashMap(it)) }
            sqliteFavorite != null
        }
    }

    override fun saveFavorite(favorite: FavoriteReadable) = babyNameDbHelper.use {
        with(dataMapper.convertFromDomain(favorite)) {
            insert(FavoriteTable.NAME, *map.toVarargArray())
        }
    }

    override fun deleteFavorite(favorite: FavoriteReadable) = babyNameDbHelper.use {
        val where = "LOWER(${FavoriteTable.PARENT}) = '${favorite.parent.lowerCase()}' AND LOWER(${FavoriteTable.BABY_NAME}) = '${favorite.babyName.lowerCase()}'"
        deleteWhere(FavoriteTable.NAME, where)
    }

    override suspend fun getFavorites(parent: String, gender: Gender): List<FavoriteReadable> {
        return withContext(Dispatchers.IO) {
            babyNameDbHelper.use {
                val where = "LOWER(${FavoriteTable.PARENT}) = ? AND ${FavoriteTable.GENDER} = ?"
                select(FavoriteTable.NAME)
                    .whereSimple(where, parent.lowerCase(), gender.ordinal.toString())
                    .parseList { SqliteFavorite(HashMap(it)) }
            }
        }
    }

}