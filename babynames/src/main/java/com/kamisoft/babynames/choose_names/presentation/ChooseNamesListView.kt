package com.kamisoft.babynames.choose_names.presentation

import com.kamisoft.babynames.commons.presentation.BabyNamesView
import com.kamisoft.babynames.commons.domain.model.BabyName
import com.kamisoft.babynames.commons.domain.model.Gender
import com.kamisoft.babynames.choose_names.domain.model.BabyNameLikable
import com.kamisoft.babynames.commons.presentation.model.OriginFilter

interface ChooseNamesListView : BabyNamesView {

    fun initViews()

    fun updateFavoriteCounter(favoriteCount: Int)

    fun showNoFavoritesMessage()

    fun getLikedBabyNames(): List<BabyNameLikable>

    fun showSearchView()

    fun enableFilterView()

    fun disableFilterView()

    fun showParentTitle(parentName: String)

    fun showGenderTitle(gender: Gender)

    fun showFavoritesCounter()

    fun hideFavoritesCounter()

    fun showOkButton()

    fun hideOkButton()

    fun showFilterView(origins: List<OriginFilter>)

    fun showFavoritesView(favorites: List<BabyName>)

    fun showFilterShowcase()

    fun showLoading()

    fun hideLoading()

    fun showError()

    fun loadNameList()

    fun showNameList(nameList: List<BabyNameLikable>)

}