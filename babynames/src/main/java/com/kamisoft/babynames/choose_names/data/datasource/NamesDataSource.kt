package com.kamisoft.babynames.choose_names.data.datasource

import com.kamisoft.babynames.choose_names.data.entity.FireBaseBabyName
import com.kamisoft.babynames.commons.domain.model.BabyNamesResult
import com.kamisoft.babynames.commons.domain.model.Gender

interface NamesDataSource {
    suspend fun getNamesList(gender: Gender): BabyNamesResult<Exception, List<FireBaseBabyName>>

    fun increaseNameLikedCounter(gender: Gender, name: String, country: String)

    fun decreaseNameLikedCounter(gender: Gender, name: String, country: String)
}