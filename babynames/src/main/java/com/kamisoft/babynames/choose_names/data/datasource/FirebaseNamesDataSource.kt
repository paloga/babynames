package com.kamisoft.babynames.choose_names.data.datasource

import com.google.firebase.crashlytics.FirebaseCrashlytics
import com.google.firebase.database.*
import com.kamisoft.babynames.choose_names.data.entity.FireBaseBabyName
import com.kamisoft.babynames.choose_names.data.entity.FireBaseBabyNameWithFavoriteCounter
import com.kamisoft.babynames.commons.domain.model.*
import com.kamisoft.babynames.commons.extensions.singleValueEvent
import com.kamisoft.babynames.commons.extensions.upperCase
import com.kamisoft.babynames.logger.Logger
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import java.text.Collator
import java.util.*


//Proguard config for firebase real time database https://firebase.google.com/docs/database/android/start/
class FirebaseNamesDataSource : NamesDataSource {

    override suspend fun getNamesList(gender: Gender): BabyNamesResult<Exception, List<FireBaseBabyName>> {
        val firebaseBabyNamesDBReference =
            FirebaseDatabase.getInstance().reference.child(FirebaseDBCommons.Node.BABY_NAMES.toString())
        val firebaseQuery =
            firebaseBabyNamesDBReference.child(FirebaseDBCommons.Node.valueOf(gender.toString().upperCase()).toString())

        return withContext(Dispatchers.IO) {
            when (val query = firebaseQuery.singleValueEvent()) {
                is BabyNamesResult.Failure -> {
                    val exception = query.failure.toException()
                    FirebaseCrashlytics.getInstance().recordException(exception)
                    Logger.error(exception, "Error on getNamesList")
                    exception.failure()
                }
                is BabyNamesResult.Success -> {
                    val list = query.success.children.mapNotNull {
                        val firebaseBabyName = it.getValue(FireBaseBabyName::class.java)
                        firebaseBabyName?.name = it.key ?: ""
                        return@mapNotNull firebaseBabyName
                    }

                    // This strategy means it'll ignore the accents. Maybe better way to do this in kotlin?
                    Collections.sort(list) { babyName1, babyName2 ->
                        val collator = Collator.getInstance()
                        collator.strength = Collator.PRIMARY
                        collator.compare(babyName1.name, babyName2.name)
                    }
                    list.success()
                }
            }
        }
    }

    override fun increaseNameLikedCounter(gender: Gender, name: String, country: String) {
        updateNameLikedCounter(gender = gender, name = name, liked = true, country = country)
    }

    override fun decreaseNameLikedCounter(gender: Gender, name: String, country: String) {
        updateNameLikedCounter(gender = gender, name = name, liked = false, country = country)
    }

    private fun updateNameLikedCounter(gender: Gender, name: String, liked: Boolean, country: String) {
        val firebaseBabyNamesDBReference =
            FirebaseDatabase.getInstance().reference.child(FirebaseDBCommons.Node.BABY_NAMES.toString())
        val firebaseQuery =
            firebaseBabyNamesDBReference.child(FirebaseDBCommons.Node.valueOf(gender.toString().upperCase()).toString())
        val firebaseNameNode = firebaseQuery.child(name)
        firebaseNameNode.runTransaction(object : Transaction.Handler {
            override fun doTransaction(mutableData: MutableData): Transaction.Result {
                val babyName = mutableData.getValue(FireBaseBabyNameWithFavoriteCounter::class.java)
                if (babyName != null) {
                    val favoritesInCountry = babyName.favorites[country.upperCase()] ?: 0
                    if (liked) {
                        babyName.favoriteCount++
                        babyName.favorites[country.upperCase()] = favoritesInCountry + 1
                    } else {
                        babyName.favoriteCount = Math.max(0, babyName.favoriteCount - 1)
                        babyName.favorites[country.upperCase()] = Math.max(0, favoritesInCountry - 1)
                    }
                    mutableData.value = babyName
                }
                return Transaction.success(mutableData)
            }

            override fun onComplete(databaseError: DatabaseError?, committed: Boolean, dataSnapshot: DataSnapshot?) {
                if (databaseError != null) {
                    Logger.error(databaseError.toException(), "Error in onComplete")
                } else {
                    Logger.debug("onComplete success")
                }
            }
        })
    }
}