package com.kamisoft.babynames.choose_names.data.repository

import com.kamisoft.babynames.choose_names.data.datasource.NamesDataFactory
import com.kamisoft.babynames.choose_names.data.entity.FireBaseBabyName
import com.kamisoft.babynames.commons.domain.model.Gender
import com.kamisoft.babynames.choose_names.domain.repository.NamesRepository
import com.kamisoft.babynames.commons.domain.model.BabyNamesResult

class NamesDataRepository(private val namesDataFactory: NamesDataFactory) : NamesRepository {

    override suspend fun getAllNamesByGender(gender: Gender): BabyNamesResult<Exception, List<FireBaseBabyName>> {
        val namesDataSource = namesDataFactory.create()
        return namesDataSource.getNamesList(gender)
    }

    override fun increaseNameLikedCounter(gender: Gender, name: String, country:String) {
        val namesDataSource = namesDataFactory.create()
        return namesDataSource.increaseNameLikedCounter(gender, name, country)
    }

    override fun decreaseNameLikedCounter(gender: Gender, name: String, country:String) {
        val namesDataSource = namesDataFactory.create()
        return namesDataSource.decreaseNameLikedCounter(gender, name, country)
    }
}