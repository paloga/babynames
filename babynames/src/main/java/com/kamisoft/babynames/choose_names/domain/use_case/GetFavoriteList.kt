package com.kamisoft.babynames.choose_names.domain.use_case

import com.kamisoft.babynames.choose_names.data.db.DbDataMapper
import com.kamisoft.babynames.commons.domain.model.Favorite
import com.kamisoft.babynames.commons.domain.model.Gender
import com.kamisoft.babynames.choose_names.domain.repository.FavoritesRepository

class GetFavoriteList(private val favoritesRepository: FavoritesRepository) {

    suspend operator fun invoke(parent: String, gender: Gender): List<Favorite> {
        return getFavorites(parent, gender)
    }

    private suspend fun getFavorites(parent: String, gender: Gender): List<Favorite> {
        val dbDataMapper = DbDataMapper()
        return favoritesRepository.getFavorites(parent, gender).map { dbDataMapper.convertToDomain(it) }
    }

}