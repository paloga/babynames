package com.kamisoft.babynames.choose_names.data.entity

import com.kamisoft.babynames.commons.domain.model.FavoriteReadable
import java.util.*

class SqliteFavorite(val map: MutableMap<String, Any?>): FavoriteReadable {
    var _id: Long by map
    override var parent: String by map
    override var gender: Int by map
    override var babyName: String by map
    override var meaning: String by map
    override var origin: String by map

    constructor(parent: String, gender: Int, babyName: String, meaning: String, origin: String)
            : this(HashMap()) {
        this.parent = parent
        this.gender = gender
        this.babyName = babyName
        this.meaning = meaning
        this.origin = origin
    }
}