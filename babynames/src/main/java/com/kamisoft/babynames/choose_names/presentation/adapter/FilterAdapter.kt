package com.kamisoft.babynames.choose_names.presentation.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.kamisoft.babynames.commons.presentation.model.OriginFilter
import com.kamisoft.babynames.databinding.RowFilterBinding
import com.simplecityapps.recyclerview_fastscroll.views.FastScrollRecyclerView

class FilterAdapter(val origins: List<OriginFilter>) :
    RecyclerView.Adapter<FilterAdapter.ViewHolder>(),
    FastScrollRecyclerView.SectionedAdapter {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val binding = RowFilterBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return ViewHolder(binding)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) = holder.bind(origins[position])

    override fun getItemCount() = origins.size

    private fun getInitialLetter(origin: String) = origin[0].toString()

    override fun getSectionName(position: Int): String = getInitialLetter(origins[position].origin)

    inner class ViewHolder(private val binding: RowFilterBinding) : RecyclerView.ViewHolder(binding.root) {

        fun bind(origin: OriginFilter) = with(itemView) {
            binding.chkOrigin.text = origin.origin
            binding.chkOrigin.isChecked = origin.selected
            binding.txtCount.text = origin.count.toString()

            binding.chkOrigin.setOnCheckedChangeListener { checkBox, checked ->
                findOriginByName(checkBox.text.toString())?.selected = checked
            }
        }

        private fun findOriginByName(name: String) = origins.find { it.origin.equals(name, ignoreCase = true) }
    }

    fun selectAll() {
        origins.forEach { it.selected = true }
        notifyDataSetChanged()
    }

    fun unSelectAll() {
        origins.forEach { it.selected = false }
        notifyDataSetChanged()
    }

    fun areAllSelected() = origins.count { it.selected }.minus(origins.size) == 0
}