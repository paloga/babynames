package com.kamisoft.babynames.choose_names.data.datasource

class FavoritesDataFactory {

    fun create(): FavoritesDataSource = SqliteFavoritesDataSource()

}