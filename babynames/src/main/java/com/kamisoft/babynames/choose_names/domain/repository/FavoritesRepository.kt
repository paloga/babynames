package com.kamisoft.babynames.choose_names.domain.repository

import com.kamisoft.babynames.commons.domain.model.BabyNamesResult
import com.kamisoft.babynames.commons.domain.model.Favorite
import com.kamisoft.babynames.commons.domain.model.FavoriteReadable
import com.kamisoft.babynames.commons.domain.model.Gender

interface FavoritesRepository {

    fun isFavorite(parent: String, gender: Gender, name: String): Boolean

    fun saveOrRemoveFavoriteName(favorite: Favorite)

    suspend fun getFavorites(parent: String, gender: Gender): List<FavoriteReadable>
}