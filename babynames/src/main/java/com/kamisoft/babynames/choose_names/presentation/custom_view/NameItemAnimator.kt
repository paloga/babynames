package com.kamisoft.babynames.choose_names.presentation.custom_view

import android.animation.Animator
import android.animation.AnimatorListenerAdapter
import android.animation.AnimatorSet
import android.animation.ObjectAnimator
import android.view.animation.AccelerateInterpolator
import android.view.animation.OvershootInterpolator
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.RecyclerView
import com.kamisoft.babynames.R
import com.kamisoft.babynames.choose_names.presentation.adapter.NamesAdapter
import org.jetbrains.anko.imageResource
import java.util.*

class NameItemAnimator : DefaultItemAnimator() {

    private var likeAnimationsMap: Map<RecyclerView.ViewHolder, AnimatorSet> = HashMap()
    internal var heartAnimationsMap: MutableMap<RecyclerView.ViewHolder, AnimatorSet> = HashMap()

    override fun canReuseUpdatedViewHolder(viewHolder: RecyclerView.ViewHolder) = true

    override fun canReuseUpdatedViewHolder(viewHolder: RecyclerView.ViewHolder, payloads: MutableList<Any>) = true

    override fun recordPreLayoutInformation(
        state: RecyclerView.State,
        viewHolder: RecyclerView.ViewHolder,
        changeFlags: Int,
        payloads: List<Any>
    ): ItemHolderInfo {
        if (changeFlags == RecyclerView.ItemAnimator.FLAG_CHANGED) {
            payloads
                .filterIsInstance<String>()
                .forEach { return NameItemHolderInfo(it) }
        }

        return super.recordPreLayoutInformation(state, viewHolder, changeFlags, payloads)
    }

    override fun animateChange(
        oldHolder: RecyclerView.ViewHolder,
        newHolder: RecyclerView.ViewHolder,
        preInfo: ItemHolderInfo,
        postInfo: ItemHolderInfo
    ): Boolean {
        cancelCurrentAnimationIfExists(newHolder)

        val holder = newHolder as NamesAdapter.NameViewHolder
        if (preInfo is NameItemHolderInfo) {
            if (NamesAdapter.ACTION_LIKE_BUTTON_CLICKED == preInfo.updateAction) {
                animateHeartButtonLiked(holder)
            } else {
                animateHeartButtonUnLiked(holder)
            }
            return true
        }

        return super.animateChange(oldHolder, newHolder, preInfo, postInfo)
    }

    private fun cancelCurrentAnimationIfExists(item: RecyclerView.ViewHolder) {
        if (likeAnimationsMap.containsKey(item)) {
            likeAnimationsMap[item]?.cancel()
        }
        if (heartAnimationsMap.containsKey(item)) {
            heartAnimationsMap[item]?.cancel()
        }
    }

    private fun animateHeartButtonLiked(holder: NamesAdapter.NameViewHolder) {
        val animatorSet = AnimatorSet()

        val rotationAnim = ObjectAnimator.ofFloat(holder.binding.btnLike, "rotation", 0f, 360f)
        rotationAnim.duration = 300
        rotationAnim.interpolator = ACCELERATE_INTERPOLATOR

        val bounceAnimX = ObjectAnimator.ofFloat(holder.binding.btnLike, "scaleX", 0.2f, 1f)
        bounceAnimX.duration = 300
        bounceAnimX.interpolator = OVERSHOOT_INTERPOLATOR

        val bounceAnimY = ObjectAnimator.ofFloat(holder.binding.btnLike, "scaleY", 0.2f, 1f)
        bounceAnimY.duration = 300
        bounceAnimY.interpolator = OVERSHOOT_INTERPOLATOR
        bounceAnimY.addListener(object : AnimatorListenerAdapter() {
            override fun onAnimationStart(animation: Animator) {
                holder.binding.btnLike.imageResource = R.drawable.ic_heart_red
            }

            override fun onAnimationEnd(animation: Animator) {
                heartAnimationsMap.remove(holder)
                dispatchChangeFinishedIfAllAnimationsEnded(holder)
            }
        })

        animatorSet.play(bounceAnimX).with(bounceAnimY).after(rotationAnim)
        animatorSet.start()

        heartAnimationsMap[holder] = animatorSet
    }

    private fun animateHeartButtonUnLiked(holder: NamesAdapter.NameViewHolder) {
        val animatorSet = AnimatorSet()

        val rotationAnim = ObjectAnimator.ofFloat(holder.binding.btnLike, "rotation", 360f, 0f)
        rotationAnim.duration = 300
        rotationAnim.interpolator = ACCELERATE_INTERPOLATOR

        val bounceAnimX = ObjectAnimator.ofFloat(holder.binding.btnLike, "scaleX", 0.9f, 1f)
        bounceAnimX.duration = 300
        bounceAnimX.interpolator = OVERSHOOT_INTERPOLATOR

        val bounceAnimY = ObjectAnimator.ofFloat(holder.binding.btnLike, "scaleY", 0.9f, 1f)
        bounceAnimY.duration = 300
        bounceAnimY.interpolator = OVERSHOOT_INTERPOLATOR
        bounceAnimY.addListener(object : AnimatorListenerAdapter() {
            override fun onAnimationStart(animation: Animator) {
                holder.binding.btnLike.imageResource = R.drawable.ic_heart_outline_grey
            }

            override fun onAnimationEnd(animation: Animator) {
                heartAnimationsMap.remove(holder)
                dispatchChangeFinishedIfAllAnimationsEnded(holder)
            }
        })

        animatorSet.play(bounceAnimX).with(bounceAnimY).after(rotationAnim)
        animatorSet.start()

        heartAnimationsMap[holder] = animatorSet
    }

    private fun dispatchChangeFinishedIfAllAnimationsEnded(holder: NamesAdapter.NameViewHolder) {
        if (likeAnimationsMap.containsKey(holder) || heartAnimationsMap.containsKey(holder)) {
            return
        }

        dispatchAnimationFinished(holder)
    }

    override fun endAnimation(item: RecyclerView.ViewHolder) {
        super.endAnimation(item)
        cancelCurrentAnimationIfExists(item)
    }

    override fun endAnimations() {
        super.endAnimations()
        for (animatorSet in likeAnimationsMap.values) {
            animatorSet.cancel()
        }
    }

    class NameItemHolderInfo(val updateAction: String) : ItemHolderInfo()

    companion object {
        private val ACCELERATE_INTERPOLATOR = AccelerateInterpolator()
        private val OVERSHOOT_INTERPOLATOR = OvershootInterpolator(4f)
    }
}
