package com.kamisoft.babynames.choose_names.di

import android.view.View
import com.kamisoft.babynames.choose_names.presentation.custom_view.babyNamesSearchView.BabyNamesSearchView
import com.kamisoft.babynames.choose_names.presentation.custom_view.babyNamesSearchView.BabyNamesSearchViewView
import dagger.Binds
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ViewComponent

@InstallIn(ViewComponent::class)
@Module
abstract class BabyNamesSearchViewModule {
    @Binds
    abstract fun bindView(view: BabyNamesSearchView): BabyNamesSearchViewView
}

@InstallIn(ViewComponent::class)
@Module
object BabyNamesSearchViewViewModule {
    @Provides
    fun bindSearchView(view: View): BabyNamesSearchView = view as BabyNamesSearchView
}