package com.kamisoft.babynames.choose_names.presentation

import com.kamisoft.babynames.choose_names.domain.model.BabyNameLikable
import com.kamisoft.babynames.choose_names.domain.use_case.GetFavoriteList
import com.kamisoft.babynames.choose_names.domain.use_case.GetNameList
import com.kamisoft.babynames.choose_names.domain.use_case.SaveFavoriteName
import com.kamisoft.babynames.commons.domain.model.*
import com.kamisoft.babynames.commons.presentation.BabyNamesPresenter
import com.kamisoft.babynames.commons.presentation.model.OriginFilter
import com.kamisoft.babynames.logger.Logger
import com.kamisoft.babynames.tracking.TrackerConstants
import com.kamisoft.babynames.tracking.TrackerManager
import kotlinx.coroutines.launch
import java.text.Collator
import javax.inject.Inject

class ChooseNamesListPresenter @Inject constructor(
    private val view: ChooseNamesListView?,
    private val getNamesUseCase: GetNameList,
    private val getFavoritesUseCase: GetFavoriteList,
    private val saveFavoriteUseCase: SaveFavoriteName,
    private val trackerManager: TrackerManager
) : BabyNamesPresenter(view) {

    private val favorites: MutableList<BabyName> = ArrayList()
    private lateinit var mode: Mode
    private lateinit var gender: Gender
    private var parentName: String? = null
    private lateinit var screenToTrack: TrackerConstants.Section.ChooseNamesList
    private lateinit var originsFilter: List<OriginFilter>

    fun onInit(mode: Mode, gender: Gender, parentName: String?) {
        this.mode = mode
        this.gender = gender
        this.parentName = parentName

        when (gender) {
            Gender.FEMALE -> trackGirlPage()
            Gender.MALE -> trackBoyPage()
        }

        view?.run {
            initViews()
            if (mode == Mode.WITH_FAVORITES) {
                showFavoritesCounter()
                parentName?.let { showParentTitle(parentName) }
                showOkButton()
            } else {
                hideFavoritesCounter()
                showGenderTitle(gender)
                hideOkButton()
            }
            showLoading()
            loadNameList()
        }
    }

    fun loadNames(origins: List<OriginFilter>? = null) {
        Logger.debug("Loading names...")
        presenterScope.launch {
            when (val namesResult = getNamesUseCase(gender)) {
                is BabyNamesResult.Failure -> onNamesLoadFailed(namesResult.failure)
                is BabyNamesResult.Success -> {
                    val names = namesResult.success
                    if (origins != null) {
                        onNamesLoadedSuccessfully(filterNamesByOrigins(names.toBabyNameLikableList(), origins))
                    } else {
                        onNamesLoadedSuccessfully(names.toBabyNameLikableList())
                        loadAllOrigins(names)
                    }
                }
            }
        }
    }

    private fun filterNamesByOrigins(
        namesList: List<BabyNameLikable>,
        origins: List<OriginFilter>
    ): List<BabyNameLikable> {
        return namesList.filter {
            val nameOrigin = it.origin
            origins.find { it.origin.equals(nameOrigin, ignoreCase = true) }?.selected ?: false
        }
    }

    private fun onNamesLoadedSuccessfully(namesList: List<BabyNameLikable>) {
        Logger.debug("onNamesLoadedSuccessfully: ${namesList.size} names")
        loadFavorites(namesList)
    }

    private fun onNamesLoadFailed(exception: Exception) {
        Logger.warning("onNamesLoadFailed: $exception")
        view?.showError()
    }

    private fun loadFavorites(namesList: List<BabyNameLikable>) {
        Logger.debug("Loading favorites...")
        parentName?.let {
            presenterScope.launch {
                val favorites = getFavoritesUseCase(it, gender)
                onFavoritesLoaded(namesList, favorites)
            }
        }
    }

    private fun onFavoritesLoaded(namesList: List<BabyNameLikable>, favoriteList: List<Favorite>) {
        Logger.debug("onFavoritesLoaded: ${favoriteList.size} favorites")
        favorites.clear()
        favorites.addAll(favoriteList.map { BabyName(it.babyName, it.origin, it.meaning) })

        favorites.forEach { favorite ->
            val babyName = namesList.find { it.name == favorite.name }
            babyName?.liked = true
        }
        view?.run {
            hideLoading()
            if (namesList.isNotEmpty()) {
                showNameList(namesList)
                updateFavoriteCounter(favorites.size)
                enableFilterView()
                showFilterShowcase()
            } else {
                showError()
            }
        }
    }

    fun onSearchClicked() {
        trackSearchEvent()
        view?.showSearchView()
    }

    private fun loadAllOrigins(namesList: List<BabyName>) {
        // This strategy mean it'll ignore the accents
        val collator = Collator.getInstance()
        collator.strength = Collator.PRIMARY
        val originsSorted = namesList.groupBy { it.origin }.toSortedMap(collator)

        originsFilter = originsSorted.map { OriginFilter(it.key, it.value.size) }
    }

    fun onFilterClicked() {
        trackFilterEvent()
        view?.showFilterView(originsFilter)
    }

    fun onFavoritesClicked() {
        trackShowFavoriteListEvent()
        view?.showFavoritesView(favorites)
    }

    fun onOriginFilterSelected(origins: List<OriginFilter>) {
        view?.showLoading()
        originsFilter = origins
        loadNames(originsFilter)
    }

    fun manageBabyNameClick(babyName: BabyNameLikable) {
        if (mode == Mode.WITH_FAVORITES) {
            parentName?.let {
                saveFavoriteUseCase.saveFavoriteName(it, gender, babyName.name, babyName.meaning, babyName.origin)
            }
            babyName.liked = !babyName.liked

            if (babyName.liked) {
                trackFavoriteEvent()
                favorites.add(BabyName(babyName.name, babyName.origin, babyName.meaning))
                saveFavoriteUseCase.increaseNameLikedCounter(gender, babyName.name)
            } else {
                trackUnFavoriteEvent()
                favorites.remove(favorites.find { it.name == babyName.name })
                saveFavoriteUseCase.decreaseNameLikedCounter(gender, babyName.name)
            }
            view?.updateFavoriteCounter(favorites.size)
        }
    }

    private fun trackBoyPage() {
        screenToTrack = TrackerConstants.Section.ChooseNamesList.BOY_NAMES
        trackerManager.sendScreen(
            TrackerConstants.Section.CHOOSE_NAMES_LIST.value,
            screenToTrack.value
        )
    }

    private fun trackGirlPage() {
        screenToTrack = TrackerConstants.Section.ChooseNamesList.GIRL_NAMES
        trackerManager.sendScreen(
            TrackerConstants.Section.CHOOSE_NAMES_LIST.value,
            screenToTrack.value
        )
    }

    private fun trackEvent(
        screen: TrackerConstants.Section.ChooseNamesList,
        label: String
    ) {
        trackerManager.sendEvent(
            category = screen.value,
            action = TrackerConstants.Action.CLICK.value,
            label = label
        )
    }

    private fun trackFavoriteEvent() {
        trackEvent(screenToTrack, TrackerConstants.Label.ChooseNameListScreen.FAVORITE_NAME.value)
    }

    private fun trackUnFavoriteEvent() {
        trackEvent(screenToTrack, TrackerConstants.Label.ChooseNameListScreen.UNFAVORITE_NAME.value)
    }

    private fun trackSearchEvent() {
        trackEvent(screenToTrack, TrackerConstants.Label.ChooseNameListScreen.SEARCH.value)
    }

    private fun trackFilterEvent() {
        trackEvent(screenToTrack, TrackerConstants.Label.ChooseNameListScreen.FILTER.value)
    }

    private fun trackShowFavoriteListEvent() {
        trackEvent(screenToTrack, TrackerConstants.Label.ChooseNameListScreen.SHOW_FAVORITES.value)
    }

    enum class Mode(val value: Int) {
        WITH_FAVORITES(1),
        JUST_NAME_LIST(2);

        companion object {
            fun get(searchedValue: Int?): Mode = values().find { it.value == searchedValue } ?: WITH_FAVORITES
        }
    }
}