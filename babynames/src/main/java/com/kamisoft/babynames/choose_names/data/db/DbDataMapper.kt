package com.kamisoft.babynames.choose_names.data.db

import com.kamisoft.babynames.choose_names.data.entity.SqliteFavorite
import com.kamisoft.babynames.commons.domain.model.Favorite
import com.kamisoft.babynames.commons.domain.model.FavoriteReadable

class DbDataMapper {

    fun convertFromDomain(favorite: FavoriteReadable) = with(favorite) {
        SqliteFavorite(parent, gender, babyName, meaning, origin)
    }

    fun convertToDomain(sqliteFavorite: FavoriteReadable) = with(sqliteFavorite) {
        Favorite(parent, gender, babyName, meaning, origin)
    }

}