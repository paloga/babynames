package com.kamisoft.babynames.choose_names.domain.model

data class BabyNameLikable(val name: String, val origin: String, val meaning: String, var liked: Boolean)