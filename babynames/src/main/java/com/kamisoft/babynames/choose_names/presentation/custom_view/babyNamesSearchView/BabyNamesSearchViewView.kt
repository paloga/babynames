package com.kamisoft.babynames.choose_names.presentation.custom_view.babyNamesSearchView

import com.kamisoft.babynames.commons.presentation.BabyNamesView

interface BabyNamesSearchViewView : BabyNamesView {

    fun initSearchView()

    fun doSearch(query: String, searchCallback: (String) -> Unit)

    fun clearFocus()

    fun clearSearchText()
}