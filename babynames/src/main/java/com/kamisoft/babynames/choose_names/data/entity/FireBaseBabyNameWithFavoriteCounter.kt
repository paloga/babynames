package com.kamisoft.babynames.choose_names.data.entity

import com.google.firebase.database.IgnoreExtraProperties

@IgnoreExtraProperties
class FireBaseBabyNameWithFavoriteCounter {
    lateinit var gender: String
    lateinit var origin: String
    lateinit var meaning: String
    var favoriteCount: Int = 0
    var favorites: HashMap<String, Int> = HashMap()
}