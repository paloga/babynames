package com.kamisoft.babynames.choose_names.data.db


object FavoriteTable {
    const val NAME = "Favorite"
    const val ID = "_id"
    const val PARENT = "parent"
    const val GENDER = "gender"
    const val BABY_NAME = "babyName"
    const val MEANING = "meaning"
    const val ORIGIN = "origin"
}