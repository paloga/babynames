package com.kamisoft.babynames.choose_names.domain.use_case

import com.kamisoft.babynames.choose_names.data.entity.toBabyNames
import com.kamisoft.babynames.choose_names.domain.repository.NamesRepository
import com.kamisoft.babynames.commons.domain.model.*

class GetNameList(private val namesRepository: NamesRepository) {

    suspend operator fun invoke(gender: Gender): BabyNamesResult<Exception, List<BabyName>> {
        return getNames(gender)
    }

    private suspend fun getNames(gender: Gender): BabyNamesResult<Exception, List<BabyName>> {
        return when (val dataResult = namesRepository.getAllNamesByGender(gender)) {
            is BabyNamesResult.Failure -> BabyNamesResult.Failure(dataResult.failure)
            is BabyNamesResult.Success -> BabyNamesResult.Success(dataResult.success.toBabyNames())
        }
    }

}