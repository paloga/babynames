package com.kamisoft.babynames.choose_names.domain.use_case

import com.kamisoft.babynames.commons.domain.model.Favorite
import com.kamisoft.babynames.commons.domain.model.Gender
import com.kamisoft.babynames.choose_names.domain.repository.FavoritesRepository
import com.kamisoft.babynames.choose_names.domain.repository.NamesRepository
import java.util.*

class SaveFavoriteName(private val favoriteRepository: FavoritesRepository, private val namesRepository: NamesRepository) {

    fun saveFavoriteName(parent: String, gender: Gender, name: String, meaning: String, origin: String) {
        val favorite = Favorite(parent, gender = gender.ordinal, babyName = name, meaning = meaning, origin = origin)
        favoriteRepository.saveOrRemoveFavoriteName(favorite)
    }

    fun increaseNameLikedCounter(gender: Gender, name: String) {
        namesRepository.increaseNameLikedCounter(gender, name, getCurrentCountryFromLocale())
    }

    fun decreaseNameLikedCounter(gender: Gender, name: String) {
        namesRepository.decreaseNameLikedCounter(gender, name, getCurrentCountryFromLocale())
    }

    private fun getCurrentCountryFromLocale() = Locale.getDefault().country
}