package com.kamisoft.babynames.choose_names.presentation

import android.annotation.SuppressLint
import android.app.Activity
import android.util.TypedValue
import android.view.Menu
import android.view.MenuItem
import android.view.View
import androidx.recyclerview.widget.LinearLayoutManager
import com.afollestad.materialdialogs.MaterialDialog
import com.afollestad.materialdialogs.customview.customView
import com.google.android.gms.ads.AdListener
import com.google.android.gms.ads.AdRequest
import com.kamisoft.babynames.R
import com.kamisoft.babynames.choose_names.domain.model.BabyNameLikable
import com.kamisoft.babynames.choose_names.presentation.ChooseNamesListPresenter.Mode
import com.kamisoft.babynames.choose_names.presentation.adapter.NamesAdapter
import com.kamisoft.babynames.choose_names.presentation.custom_view.NameItemAnimator
import com.kamisoft.babynames.choose_names.presentation.custom_view.babyNamesSearchView.BabyNamesSearchView
import com.kamisoft.babynames.commons.Constants
import com.kamisoft.babynames.commons.OSVersionUtils
import com.kamisoft.babynames.commons.domain.model.BabyName
import com.kamisoft.babynames.commons.domain.model.Gender
import com.kamisoft.babynames.commons.extensions.*
import com.kamisoft.babynames.commons.presentation.BabyNamesActivity
import com.kamisoft.babynames.commons.presentation.custom_view.FavoritesView
import com.kamisoft.babynames.commons.presentation.custom_view.FilterView
import com.kamisoft.babynames.commons.presentation.model.OriginFilter
import com.kamisoft.babynames.databinding.ActivityNamesListBinding
import com.kamisoft.babynames.logger.Logger
import dagger.hilt.android.AndroidEntryPoint
import me.toptas.fancyshowcase.FancyShowCaseView
import javax.inject.Inject
import kotlin.properties.ReadOnlyProperty
import kotlin.reflect.KProperty

@AndroidEntryPoint
class ChooseNamesListActivity : BabyNamesActivity(), ChooseNamesListView {

    @Inject
    override lateinit var presenter: ChooseNamesListPresenter

    private lateinit var binding: ActivityNamesListBinding

    private lateinit var searchMenu: Menu
    private lateinit var searchItem: MenuItem
    private lateinit var filterItem: MenuItem

    private val babyNamesSearchView by lazy { createBabyNamesSearchView() }
    private val filterShowCaseView: FancyShowCaseView? by lazy {
        FancyShowCaseView.Builder(this)
            .focusOn(findViewById(filterItem.itemId))
            .title(getString(R.string.filter_showcase_text))
            .titleSize(16, TypedValue.COMPLEX_UNIT_SP)
            .backgroundColor(color(R.color.primary_dark))
            .showOnce(Constants.Ids.FILTER_SHOWCASE)
            .build()
    }

    companion object {
        const val ARG_GENDER = "gender"
        const val ARG_PARENT = "parent"
        const val REMOVE_ADS = "remove_ads"
        const val MODE = "mode"
    }

    private val gender: Gender by GenderArgument(ARG_GENDER)
    private val parentName: String by ParentArgument(ARG_PARENT)
    private val removeAds: Boolean by BooleanArgument(REMOVE_ADS)
    private val mode: Mode by ModeArgument(MODE)

    private val namesAdapter by lazy {
        NamesAdapter(mode) {
            Logger.debug("${it.name} Clicked")
            presenter.manageBabyNameClick(it)
        }
    }

    override fun initViewBinding(): View {
        binding = ActivityNamesListBinding.inflate(layoutInflater)
        return binding.root
    }

    override fun initPresenter() {
        presenter.onInit(mode, gender, parentName)
    }

    override fun initViews() {
        initToolbars()
        initRecyclerView()
        babyNamesSearchView.setQueryListenerToSearchView { onSearchItem(it) }
        binding.btnOk.setOnClickListener { finish() }
    }

    private fun createBabyNamesSearchView() = searchItem.actionView as BabyNamesSearchView

    private fun initToolbars() {
        initActionToolbar()
        initSearchToolbar()
        if (removeAds) {
            hideAds()
        } else {
            loadAds()
        }
        binding.toolbar.layFavoriteCounter.setOnClickListener { presenter.onFavoritesClicked() }
    }

    private fun initActionToolbar() {
        val toolbar = binding.toolbar.root
        setSupportActionBar(toolbar)
        supportActionBar?.title = null
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayShowHomeEnabled(true)
        toolbar.setNavigationIcon(R.drawable.ic_close)
    }

    private fun initSearchToolbar() {
        val searchToolbar = binding.searchToolbar.root
        searchToolbar.inflateMenu(R.menu.menu_search)
        searchMenu = searchToolbar.menu
        searchItem = searchMenu.findItem(R.id.actionSearch)

        searchToolbar.setNavigationOnClickListener {
            hideSearchToolbar()
            babyNamesSearchView.clearSearchText()
        }

        searchItem.setOnActionExpandListener(object : MenuItem.OnActionExpandListener {
            override fun onMenuItemActionCollapse(item: MenuItem): Boolean {
                hideSearchToolbar()
                babyNamesSearchView.clearSearchText()
                return true
            }

            override fun onMenuItemActionExpand(item: MenuItem) = true
        })
    }

    @SuppressLint("NewApi")
    private fun showSearchToolbar() {
        if (OSVersionUtils.isLollipopOrGreater)
            binding.searchToolbar.root.circleReveal(posFromRight = 1, containsOverflow = true, isShow = true)
        else
            binding.searchToolbar.root.visible()
    }

    @SuppressLint("NewApi")
    private fun hideSearchToolbar() {
        if (OSVersionUtils.isLollipopOrGreater)
            binding.searchToolbar.root.circleReveal(posFromRight = 1, containsOverflow = true, isShow = false)
        else
            binding.searchToolbar.root.gone()
    }

    private fun initRecyclerView() {
        binding.rvList.layoutManager = LinearLayoutManager(this)
        binding.rvList.adapter = namesAdapter
        binding.rvList.itemAnimator = NameItemAnimator()
    }

    override fun showLoading() {
        binding.contentView.gone()
        binding.errorView.root.gone()
        binding.loadingView.root.visible()
    }

    override fun hideLoading() {
        binding.loadingView.root.gone()
        binding.errorView.root.gone()
        binding.contentView.visible()
    }

    override fun showError() {
        binding.contentView.gone()
        binding.loadingView.root.gone()
        binding.errorView.errorView.text = getString(R.string.error_get_names)
        binding.errorView.root.visible()
    }

    override fun loadNameList() {
        presenter.loadNames()
    }

    override fun showNameList(nameList: List<BabyNameLikable>) {
        binding.loadingView.root.gone()
        binding.errorView.root.gone()
        binding.contentView.visible()
        namesAdapter.setBabyNameList(nameList)
    }

    private class GenderArgument(private val arg: String) : ReadOnlyProperty<Activity, Gender> {
        override fun getValue(thisRef: Activity, property: KProperty<*>): Gender =
            Gender.valueOf(thisRef.intent.extras?.getString(arg)?.upperCase().orEmpty())
    }

    private class ParentArgument(private val arg: String) : ReadOnlyProperty<Activity, String> {
        override fun getValue(thisRef: Activity, property: KProperty<*>): String =
            thisRef.intent.extras?.getString(arg)?.upperCase().orEmpty()
    }

    private class BooleanArgument(private val arg: String) : ReadOnlyProperty<Activity, Boolean> {
        override fun getValue(thisRef: Activity, property: KProperty<*>): Boolean =
            thisRef.intent.extras?.getBoolean(arg) == true
    }

    private class ModeArgument(private val arg: String) : ReadOnlyProperty<Activity, Mode> {
        override fun getValue(thisRef: Activity, property: KProperty<*>): Mode =
            Mode.get(thisRef.intent.extras?.getInt(arg))
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        val inflater = menuInflater
        inflater.inflate(R.menu.menu_home, menu)
        filterItem = menu.findItem(R.id.actionFilter)
        disableFilterView()
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem) = when (item.itemId) {
        android.R.id.home -> {
            finish()
            true
        }
        R.id.actionSearch -> {
            presenter.onSearchClicked()
            true
        }
        R.id.actionFilter -> {
            presenter.onFilterClicked()
            true
        }
        else -> super.onOptionsItemSelected(item)
    }

    private fun onSearchItem(item: String) = findNameInList(item)

    private fun findNameInList(text: String) {
        (binding.rvList.layoutManager as LinearLayoutManager).scrollToPositionWithOffset(
            namesAdapter.getFirstItemPositionStartingWith(text),
            20
        )
    }

    override fun showSearchView() {
        showSearchToolbar()
        searchItem.expandActionView()
    }

    override fun enableFilterView() {
        filterItem.isEnabled = true
    }

    override fun disableFilterView() {
        filterItem.isEnabled = false
    }

    override fun showOkButton() {
        binding.btnOk.visible()
    }

    override fun hideOkButton() {
        binding.btnOk.gone()
    }

    override fun showFilterShowcase() {
        filterItem.let { filterShowCaseView?.show() }
    }

    override fun showParentTitle(parentName: String) {
        binding.txtChooseNamesTitle.text = getString(R.string.name_list_title, parentName)
    }

    override fun showGenderTitle(gender: Gender) {
        binding.txtChooseNamesTitle.text = when (gender) {
            Gender.FEMALE -> getString(R.string.girl_names)
            Gender.MALE -> getString(R.string.boy_names)
        }
    }

    override fun showFavoritesCounter() {
        binding.toolbar.layFavoriteCounter.visible()
    }

    override fun hideFavoritesCounter() {
        binding.toolbar.layFavoriteCounter.gone()
    }

    override fun showFilterView(origins: List<OriginFilter>) {
        val filterView = FilterView(this, removeAds)
        val dialog = MaterialDialog(this).title(R.string.filter_origin_label).customView(view = filterView)
        filterView.setOrigins(origins)
        filterView.onOkCallback = { onFilterDialogOkClicked(dialog, it) }

        dialog.show()
    }

    override fun showFavoritesView(favorites: List<BabyName>) {
        val favoritesView = FavoritesView(this, removeAds)
        val dialog = MaterialDialog(this).title(text = getString(R.string.favorites_title, parentName))
            .customView(view = favoritesView)
        favoritesView.setFavorites(favorites)
        favoritesView.onOkCallback = { dialog.dismiss() }

        dialog.show()
    }

    private fun onFilterDialogOkClicked(dialog: MaterialDialog, origins: List<OriginFilter>) {
        dialog.dismiss()
        presenter.onOriginFilterSelected(origins)
    }

    override fun updateFavoriteCounter(favoriteCount: Int) {
        binding.toolbar.tsLikesCounter.setCurrentText((favoriteCount - 1).toString())
        binding.toolbar.tsLikesCounter.setText(favoriteCount.toString())
    }

    override fun showNoFavoritesMessage() {
        snackbar(binding.contentView, R.string.name_list_no_favorites)
    }

    override fun getLikedBabyNames(): List<BabyNameLikable> = namesAdapter.getLikedBabyNames()

    private fun loadAds() {
        val adRequest = AdRequest.Builder().build()
        binding.adView.loadAd(adRequest)
        binding.adView.adListener = object : AdListener() {
            override fun onAdLoaded() {
                super.onAdLoaded()
                binding.adView.showWithAnimation()
            }
        }
    }

    private fun hideAds() {
        binding.adView.gone()
    }

    override fun onBackPressed() {
        when {
            binding.searchToolbar.root.isVisible() -> {
                hideSearchToolbar()
                babyNamesSearchView.clearSearchText()
            }
            filterShowCaseView?.isShown == true -> filterShowCaseView?.hide()
            else -> super.onBackPressed()
        }
    }
}
