package com.kamisoft.babynames.choose_names.di

import android.app.Activity
import com.kamisoft.babynames.choose_names.presentation.ChooseNamesListActivity
import com.kamisoft.babynames.choose_names.presentation.ChooseNamesListView
import dagger.Binds
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ActivityComponent

@InstallIn(ActivityComponent::class)
@Module
abstract class ChooseNamesModule {
    @Binds
    abstract fun bindView(activity: ChooseNamesListActivity): ChooseNamesListView
}

@InstallIn(ActivityComponent::class)
@Module
object ChooseNamesActivityModule {
    @Provides
    fun bindActivity(activity: Activity): ChooseNamesListActivity = activity as ChooseNamesListActivity
}