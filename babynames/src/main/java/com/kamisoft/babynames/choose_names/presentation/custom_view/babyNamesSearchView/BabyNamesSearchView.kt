package com.kamisoft.babynames.choose_names.presentation.custom_view.babyNamesSearchView

import android.content.Context
import android.graphics.Color
import android.os.Build
import android.util.AttributeSet
import android.widget.AutoCompleteTextView
import android.widget.EditText
import android.widget.ImageView
import android.widget.TextView
import androidx.annotation.RequiresApi
import androidx.appcompat.widget.SearchView
import com.kamisoft.babynames.R
import com.kamisoft.babynames.logger.Logger
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject

@AndroidEntryPoint
class BabyNamesSearchView @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyleAttr: Int = 0
) : SearchView(context, attrs, defStyleAttr), BabyNamesSearchViewView {

    @Inject
    lateinit var presenter: BabyNamesSearchViewPresenter

    override fun onAttachedToWindow() {
        super.onAttachedToWindow()
        presenter.onInit()
    }

    override fun initSearchView() {
        setBackgroundResource(androidx.appcompat.R.drawable.abc_textfield_search_default_mtrl_alpha)
        maxWidth = Integer.MAX_VALUE
        isSubmitButtonEnabled = false

        setSearchViewCloseButton()
        setSearchViewTextProperties()
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
            setSearchViewCursor()
        }
    }

    private fun setSearchViewCloseButton() {
        val closeButton = findViewById<ImageView>(androidx.appcompat.R.id.search_close_btn)
        closeButton.setImageResource(R.drawable.ic_close)
    }

    private fun setSearchViewTextProperties() {
        val txtSearch = findViewById<EditText>(androidx.appcompat.R.id.search_src_text)
        txtSearch.setHint(R.string.search_hint)
        txtSearch.setHintTextColor(Color.LTGRAY)
        txtSearch.setTextColor(Color.WHITE)
    }

    @RequiresApi(Build.VERSION_CODES.Q)
    private fun setSearchViewCursor() {
        val searchTextView = findViewById<AutoCompleteTextView>(androidx.appcompat.R.id.search_src_text)
        searchTextView.setTextCursorDrawable(R.drawable.search_cursor)
    }

    fun setQueryListenerToSearchView(searchEvent: (String) -> Unit) {
        setOnQueryTextListener(object : OnQueryTextListener {
            override fun onQueryTextSubmit(query: String): Boolean {
                presenter.onQueryTextSubmit(query, searchEvent)
                return true
            }

            override fun onQueryTextChange(newText: String): Boolean {
                presenter.onQueryTextChange(newText, searchEvent)
                return true
            }
        })
    }

    override fun doSearch(query: String, searchCallback: (String) -> Unit) {
        Logger.info("query $query")
        if (query.isNotEmpty()) searchCallback.invoke(query)
    }

    override fun clearSearchText() {
        val searchTextView = findViewById<AutoCompleteTextView>(androidx.appcompat.R.id.search_src_text)
        searchTextView.setText("")
    }
}