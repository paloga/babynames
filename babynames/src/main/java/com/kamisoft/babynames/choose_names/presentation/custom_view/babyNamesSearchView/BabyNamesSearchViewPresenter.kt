package com.kamisoft.babynames.choose_names.presentation.custom_view.babyNamesSearchView

import com.kamisoft.babynames.commons.presentation.BabyNamesPresenter
import javax.inject.Inject

class BabyNamesSearchViewPresenter @Inject constructor(private val view: BabyNamesSearchViewView?) :
    BabyNamesPresenter(view) {

    fun onInit() = view?.initSearchView()

    fun onQueryTextSubmit(query: String, searchCallback: (String) -> Unit) {
        view?.run {
            doSearch(query, searchCallback)
            clearFocus()
        }
    }

    fun onQueryTextChange(query: String, searchCallback: (String) -> Unit) {
        view?.doSearch(query, searchCallback)
    }
}
