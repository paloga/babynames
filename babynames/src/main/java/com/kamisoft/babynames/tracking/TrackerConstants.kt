package com.kamisoft.babynames.tracking

/***
 * VERY IMPORTANT:

 * ********************* FIREBASE CONSIDERATIONS ********************

 * EVENTS:
 * Event names can be up to 32 characters long, may only contain alphanumeric characters and
 * underscores ("_"), and must start with an alphabetic character. The "firebase_" prefix is
 * reserved and should not be used.

 * The following event names are reserved and cannot be used:
 * - app_clear_data
 * - app_uninstall
 * - app_update
 * - error
 * - first_open
 * - in_app_purchase
 * - notification_dismiss
 * - notification_foreground
 * - notification_open
 * - notification_receive
 * - os_update
 * - session_start
 * - user_engagement

 * PARAMS:
 * Param names can be up to 24 characters long, may only contain alphanumeric characters and
 * underscores ("_"), and must start with an alphabetic character. Param values can be up to 36
 * characters long. The "firebase_" prefix is reserved and should not be used.
 */

class TrackerConstants {

    enum class Section(val value: String) {
        MAIN("main"),
        PARENTS_SETUP("parents_setup"),
        FIND_MATCHES("find_matches"),
        CHOOSE_NAMES_LIST("choose_name_list"),
        CONTACT("contact");

        enum class Main(val value: String) {
            MAIN("s_main")
        }

        enum class ParentsSetup(val value: String) {
            MAIN("s_parents_main")
        }

        enum class FindMatches(val value: String) {
            MAIN("s_matches_main");
        }

        enum class ChooseNamesList(val value: String) {
            BOY_NAMES("s_boy_choose_names_list"),
            GIRL_NAMES("s_girl_choose_names_list"),
        }

        enum class Contact(val value: String) {
            MAIN("s_contact_main")
        }
    }

    enum class Action(val value: String) {
        CLICK("click"),
        ACCEPT("accept"),
        CANCEL("cancel"),
        FINISH("finish"),
        SHOW("show");

        companion object {
            fun get(value: String) = values().find { it.value == value } ?: CLICK
        }
    }

    enum class Param(val value: String) {
        SCREEN("screen"),
        ACTION("action");
    }

    class Label(val value: String) {

        enum class MainScreen(val value: String) {
            BOY("boy"),
            GIRL("girl"),
            MATCHES("matches"),
            PARENT1_CHOOSE("parent1_choose"),
            PARENT2_CHOOSE("parent2_choose"),
            DRAWER_PARENTS_SCREEN("menu_parents"),
            DRAWER_BOY_NAMES_SCREEN("menu_boy_names"),
            DRAWER_GIRL_NAMES_SCREEN("menu_girl_names"),
            DRAWER_CONTACT_SCREEN("menu_contact"),
            DRAWER_REMOVE_ADS("menu_remove_ads"),
            NEW_APP_VERSION("main_new_version_dialog"),
            REQUIRED_APP_VERSION("main_required_version_dialog"),
            NEW_APP_OK("main_new_version_ok"),
            NEW_APP_DISMISSED("main_new_version_dismissed"),
            REQUIRED_APP_OK("main_required_version_ok"),
            REQUIRED_APP_DISMISSED("main_required_version_dismissed"),
            SHOW_PARENT1_FAVORITES("main_show_parent1_favorites"),
            SHOW_PARENT2_FAVORITES("main_show_parent2_favorites"),
            SHARE("share")
        }

        enum class ParentsScreen(val value: String) {
            PARENT1_DAD("parent1_dad"),
            PARENT1_MOM("parent1_mom"),
            PARENT2_DAD("parent2_dad"),
            PARENT2_MOM("parent2_mom"),
            GO_OK("parents_go_ok"),
            GO_KO("parents_go_kk")
        }

        enum class ChooseNameListScreen(val value: String) {
            FAVORITE_NAME("choose_name_list_favorite_name"),
            UNFAVORITE_NAME("choose_name_list_unfavorite_name"),
            SEARCH("choose_name_list_search"),
            FILTER("choose_name_list_filter"),
            SHOW_FAVORITES("choose_name_list_show_favorites")
        }

        enum class ContactScreen(val value: String) {
            EMAIL_SUPPORT("contact_email_support")
        }
    }

}
