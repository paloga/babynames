package com.kamisoft.babynames.tracking

data class TrackerEvent(
    val category: String,
    val action: String = TrackerConstants.Action.CLICK.value,
    val label: String,
    val value: Double? = null
) {
    override fun toString(): String = label
}
